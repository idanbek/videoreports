import initialState from './initialState.js'
import { ADD, REMOVE } from './actionTypes'

export default function reducer(state = initialState, {type, payload} = {}) {

    switch (type) {

        case ADD:
            state[payload.widgetId] = payload;
            return {...state};

        case REMOVE:
            delete state[payload.widgetId];
            return {...state};

        default:
            return state;

    }
}