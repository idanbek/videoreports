import axios from 'axios'
import { forEach, range , cloneDeep, isEmpty } from 'lodash'

import { setIn, setAdBoxImage, changeVoiceOver } from '../activeVideo/actions'
import getInitialStatus from '../shared/getInitialStatus'
import { ADD, REMOVE } from './actionTypes'
import { GET_INTEGRATIONS_URL } from '../../constants/urls'

/**
 * Creates add widgetData action.
 * @param widgetData
 * @returns Object action
 */
export function addWidgetData(widgetData) {
    return {
        type: ADD,
        payload: widgetData
    };
}

/**
 * Creates remove widgetData action.
 * @param widgetData
 * @returns Object action
 */
export function removeWidgetData(widgetData) {
    return {
        type: REMOVE,
        payload: widgetData
    };
}

export function fetchWidgetDatas(datas=null) {
    return (dispatch, getState) => {
        datas = cloneDeep(datas || getState().widgetDatas);
        forEach(datas, data => {
            dispatch(fetchWidgetData(data));
        });
    }
}


export function editWidgetData(newWidgetData) {
    return (dispatch, getState) => {
        let { activeVideo:{ frames }, widgetDatas } = getState(),
            widgetIndex = frames.findIndex(({id}) => id === newWidgetData.widgetId),
            { labels, name } = frames[widgetIndex],
            { fields, ad_id } = widgetDatas[newWidgetData.widgetId];

        //get and set ad image
        if (name === 'Ad Box' && newWidgetData.ad_id !== ad_id) {
            dispatch(setAdBoxImage(newWidgetData.ad_id, ['frames', widgetIndex]))
        }
        // set isEdited to false if field is changed

        name !== 'Chart' && fields.forEach((field, idx) => {
            if (field.value !== newWidgetData.fields[idx].value) {
                labels[idx]['isEdited'] = false;
                labels[idx + 1]['isEdited'] = false;
            }
        });

        dispatch(setIn(['frames', widgetIndex, 'labels'], labels));

        if (newWidgetData.account_id) {
            dispatch(fetchWidgetData(newWidgetData));
        }
    }
}






export function fetchWidgetData(widgetData) {
    return (dispatch, getState) => {
        let { frames, dates } = getState().activeVideo;
        // set status loading
        widgetData.status = getInitialStatus({isLoading: true});
        dispatch(addWidgetData({...widgetData}));
        // remove old data and status when sending request
        delete widgetData['data'];
        delete widgetData['status'];
        // get insights
        axios.post(GET_INTEGRATIONS_URL + 'facebook/insights', {...widgetData, dates})
            .then(({data}) => {
                let widgetIndex = frames.findIndex(({id}) => id === widgetData.widgetId),
                    { labels=null, voiceOver, name } = frames[widgetIndex] || {};

                widgetData.status = getInitialStatus({isLoaded: true});
                widgetData.data = data;

                const SHOULD_UPDATE_LABELS = name !== 'Chart' && labels && !isEmpty(labels) && !isEmpty(widgetData.data);

                if (SHOULD_UPDATE_LABELS) {
                    // set labels text
                    range(0, widgetData.fields.length * 2, 2).forEach((val, idx) => {
                        let { label , value } = widgetData.data[ idx ];

                        if (labels[ val ][ 'isEdited' ]) {
                            widgetData.data[ idx ][ 'label' ] = labels[ val ][ 'text' ]
                        } else {
                            labels[ val ][ 'text' ] = label;
                        }

                        if (labels[ val + 1 ][ 'isEdited' ]) {
                            widgetData.data[ idx ][ 'speech_text' ] = labels[ val + 1 ][ 'text' ]
                        } else {
                            labels[ val + 1 ][ 'text' ] = value;
                        }
                    });
                    dispatch(setIn(['frames', widgetIndex, 'labels'], labels));
                }

                dispatch(addWidgetData({...widgetData}));

                if (name !== 'Chart') {
                    // change voice over after data been fetched
                    dispatch(changeVoiceOver(cloneDeep(voiceOver), ['frames', widgetIndex]));
                }
            });
    };
}
