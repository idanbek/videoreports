import { WIDGET_DATAS } from '../../constants/localStorage'

export default localStorage.getObject(WIDGET_DATAS) || {}