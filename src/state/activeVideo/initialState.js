import { BACKGROUNDS } from '../../constants/createReportNavs'
import { TIME_RANGES } from '../../constants/createReportNavs'
import { STORAGE_KEY } from '../../constants/localStorage'

const defaultVideo = {
    bg: {
        themeColors: ['#93c01f', 'black', ' rgb(162, 23, 128)'],
        backgroundImage: BACKGROUNDS[3].backgroundImage,

    },
    frames: [],
    dates: TIME_RANGES[0][1]
};

export default localStorage.getObject(STORAGE_KEY) || defaultVideo;