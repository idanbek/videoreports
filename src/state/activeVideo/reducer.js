import { set } from 'lodash'
import {
    SET,
    LOAD_VIDEO,
    ADD_BATCH_WIDGETS,
    SET_IN
} from './actionTypes'
import ActiveVideoUtils from './utils'
import initialState from './initialState'


export default function reducer(state = initialState,  {type, payload, path=''} = {}) {
    let placementMap = null;
    let output = null;
    let {frames} = state;


    switch (type) {

        case SET_IN:
            output = Object.assign({}, set(state, path, payload));
            return output;

        case SET:
            output = {...payload};
            return output;

        case LOAD_VIDEO:
            output = {...payload};
            return output;

        case ADD_BATCH_WIDGETS:
            frames = frames.concat(payload);
            placementMap = ActiveVideoUtils.getPlacementMap(frames);
            frames = frames.filter(widget=> widget.widgetType !== 'EMPTY' || !placementMap.hasObject(widget.position));
            output = {...state,frames};
            return output;

        default:
            return state

    }
}