export const SET_IN = 'SET_IN';
export const REMOVE = 'REMOVE';
export const SET ='SET';

export const ADD_BATCH_WIDGETS = 'ADD_BATCH_WIDGETS';

export const CHANGE_ITEM = 'CHANGE_ITEM';

export const CHANGE_VOICE_OVER = 'CHANGE_VOICE_OVER';
export const LOAD_VIDEO = 'LOAD_VIDEO';

