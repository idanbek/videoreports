import axios from 'axios'
import moment from 'moment'
import { get, reject, cloneDeep } from 'lodash'

import ActiveVideoUtils from './utils'
import { setFocusItem } from '../focusItem/actions'
import { GET_SPEECH_URL, GET_AD_PREVIEW, POST_DRAFT_URL } from '../../constants/urls'
import { ADD_BATCH_WIDGETS, LOAD_VIDEO, SET_IN } from './actionTypes.js'
import { DATE_FORMAT } from '../../constants/createReportNavs'
import { fetchWidgetDatas, removeWidgetData, addWidgetData } from '../widgetDatas/actions'
import { getWidget, WIDGETS_CONFIG, DEFAULT_FACEBOOK_FIELDS } from '../../constants/allWidgets'


export function setIn(path, payload) {
    return {
        type: SET_IN,
        path,
        payload,
    }
}

export function addBatchWidgets(payload){
    return {
        type: ADD_BATCH_WIDGETS,
        payload
    }
}

export function loadVideo(payload){
    return {
        type: LOAD_VIDEO,
        payload
    }
}

export function deleteWidget(widgetId){
    return (dispatch, getState) => {
        let { activeVideo: { frames } } = getState();
        dispatch(setFocusItem({type: 'date', path: 'dates'}));
        dispatch(setIn(['frames'], reject(frames, {id: widgetId})));
        dispatch(removeWidgetData({widgetId}));
    }
}

export function addNewWidget(name, defaultData={}) {
    return (dispatch, getState) => {
        let { activeVideo:{ frames } } = getState(),
            fieldsNum = WIDGETS_CONFIG[name].fieldsNum,
            widgetToAdd = getWidget(name);
        if (widgetToAdd) {
            if (fieldsNum) {
                let widgetData = {
                    widgetId: widgetToAdd.id,
                    account_id: null,
                    fields: DEFAULT_FACEBOOK_FIELDS.slice(0, fieldsNum),
                    ...defaultData
                };
                if (name === 'Chart') {
                    console.log('HI ')
                    widgetData['time_increment'] = 1;
                }
                dispatch(addWidgetData(widgetData))
            }
            // Position is set to bottom.
            const positionToAddWidget = {x:0,y:1 + Math.max(...frames.map(({size: {height}, position: {y}}) => y + height))};
            frames.push({...widgetToAdd,position:positionToAddWidget});
            dispatch(setIn(['frames'], frames));
            dispatch(setFocusItem({type: 'widget', path: ['frames', frames.length - 1]}));
        }
    }
}

export function changeVoiceOver(newVoice, path) {
    return async (dispatch, getState) => {
        let { widgetDatas, activeVideo } = getState(),
            { id, voiceOver } = get(activeVideo, path, {voiceOver: {}});

        newVoice.text = ActiveVideoUtils.parseVoice(newVoice.template, widgetDatas[id]);

        if (voiceOver &&
            (voiceOver.text !== newVoice.text ||
             voiceOver.languageCode !== newVoice.languageCode ||
             voiceOver.voice !== newVoice.voice ||
             voiceOver.src === null)
        ) {
            dispatch(setIn(path.concat(['voiceOver']), {...newVoice, isSaving: true}));
            let { data: newVoiceOver } = await axios.post(GET_SPEECH_URL, newVoice);
            dispatch(setIn(path.concat(['voiceOver']), {...newVoiceOver, isSaving: false}));
        }
    };
}

export function setPositions(layout) {
    return (dispatch , getState) => {
        let frames = getState().activeVideo.frames;
        frames = frames.map(widget=>{
            let { x, y } = layout.find(layoutItem => layoutItem.i == widget.id);
            return {...widget, position:{x, y}}
        });
        dispatch(setIn('frames', frames));
    }
}

export function setDates(newDates) {
    return (dispatch, getState) => {
        let frames = getState().activeVideo.frames,
            tempDates = [
                moment(newDates[0], DATE_FORMAT).format('D.M'),
                moment(newDates[1], DATE_FORMAT).format('D.M')
            ];

        frames = frames.map((widget) => {
            if (widget.name !== 'EMPTY') {
                widget.labels = widget.labels.map(label => {
                    if (label.timePicker) {
                        label.text = `${tempDates[0]}-${tempDates[1]}`;
                    }
                    return label;
                });
            }
            return widget;
        });

        dispatch(setIn('frames', frames));
        dispatch(setIn('dates', newDates));
        dispatch(fetchWidgetDatas());
    };
}

export function generateVideoID() {
    return async (dispatch) => {
        let {data: {video_id}} = await axios.post(POST_DRAFT_URL, {video_json: {}});
        dispatch(setIn('video_id', video_id));
    };
}

export function setAdBoxImage(adId, path) {
    return async (dispatch, getState) => {
        let decoratorPath = path.concat(['decorators', 0]),
            decorator = get(getState().activeVideo, decoratorPath),
            oldImage = get(decorator, ['style', 'backgroundImage'], '').replace('url(', '').replace(')', '');
        dispatch(setIn(decoratorPath, {...decorator, isLoading: true}));
        try {
            let { data } = await axios.post(GET_AD_PREVIEW + adId, {old_image: oldImage});
            dispatch(setIn(decoratorPath, {...decorator, isLoading: false, style:{backgroundImage: `url(${data})`}}));
        } catch (e) {
            dispatch(setIn(decoratorPath, {...decorator, isLoading: false}));
        }
    };
}
