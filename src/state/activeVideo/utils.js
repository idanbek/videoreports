import { cloneDeep } from 'lodash'
import { NUM_OF_COLUMNS } from '../../constants/video'


export default class ActiveVideoUtils {


    static round(num, pre) {
        if (!pre) pre = 0;
        let pow = Math.pow(10, pre);
        return Math.round(num * pow) / pow;
    }

    static parseVoice(rawText, widgetData) {
        let regexp = /\[metric\[\d+]\.(value|name)]/g,
            toChange = rawText.match(regexp),
            outputText = rawText;

        if (toChange && widgetData && widgetData.fields) {
            toChange.forEach((pattern) => {
                let numRegex = /\[(\d+)]/,
                    insightIndex = Number(numRegex.exec(pattern)[1]);
                if (insightIndex < widgetData.fields.length) {
                    let data = widgetData['data'][insightIndex];
                    if (pattern.endsWith('value]')) {
                        outputText = outputText.replace(pattern, data.speech_text);
                    } else {
                        outputText = outputText.replace(pattern, data.label);
                    }
                }
            });
        }
        return outputText;
    }

    static getPlacementMap(widgets, includeEmpty = false) {
        let placementMap = new Map();

        placementMap.setObject = function (key, value) {
            return this.set(JSON.stringify(key), value)
        };

        placementMap.getObject = function (key) {
            return this.get(JSON.stringify(key))
        };

        placementMap.hasObject = function (key) {
            return this.has(JSON.stringify(key))
        };

        widgets.forEach((widget) => {
            let {position: {x, y}, size: {height, width}} = widget;

            for (let i = x; i < x + width; i++) {
                for (let j = y; j < y + height; j++) {
                    placementMap.setObject({x: i, y: j}, widget);
                }
            }
        });

        return placementMap;
    }

    static canPlace(widget, position, placementMap) {
        let { x, y } = position,
            flag = true,
            {size: { height, width }} = widget;

        if (x < 0 || y < 0) {
            return false
        }

        for (let i = x; i < x + width && flag; i++) {
            flag = i < NUM_OF_COLUMNS && !placementMap.hasObject({x: i, y})
        }

        for (let i = y; i < y + height && flag; i++) {
            flag = !placementMap.hasObject({x, y: i})
        }

        return flag;
    }

    static getAvailableSpot(widget, placementMap) {
        for (let i = 0; i < 10; i++) {
            for (let j = 0; j < NUM_OF_COLUMNS; j++) {
                if (ActiveVideoUtils.canPlace(widget, {x: j, y: i}, placementMap)) {
                    return {x: j, y: i};
                }
            }
        }
        throw new Error('Cannot get available space');
    }

    static injectDataInGraphs(originalJson,widgetDatas){
        let jsonCopy = cloneDeep(originalJson);

        jsonCopy.frames.forEach(widget=>{
            if(widget.graph){
                widget.graph['data'] = widgetDatas[widget.id];
            }
        });

        return jsonCopy;
    }
}