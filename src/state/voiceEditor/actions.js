import axios from 'axios'
import { SET_VOICE_EDITOR } from './actionTypes.js'
import { changeVoiceOver } from '../activeVideo/actions'
import { GET_SPEECH_URL } from '../../constants/urls'
import { IS_PLAYING, IS_LOADING } from '../../constants/pollyMapping'
import ActiveVideoUtils from '../../state/activeVideo/utils'

export function setVoiceEditor(payload) {
    return{
		type: SET_VOICE_EDITOR,
		payload: payload
	}
}

export function updateVoiceOver() {
    return (dispatch, getState) => {
        let { focusItem:{ path }, activeVideo:{ frames }, voiceEditor } = getState(),
            { languageCode, voice, template } = voiceEditor,
            { languageCode:prevLanguageCode, voice:prevVoice } = frames[path[1]].voiceOver;

        if (prevLanguageCode === languageCode && prevVoice === voice) {
            dispatch(changeVoiceOver({template, languageCode, voice}, ['frames', path[1]]));
        } else {
            frames.forEach(({voiceOver}, idx) => {
                let newTemplate = (idx === path[1]) ? template : voiceOver.template;
                dispatch(changeVoiceOver({template: newTemplate, languageCode, voice}, ['frames', idx]));
            });
        }
    }
}

export function playVoiceOver() {
    return (dispatch, getState) => {
        let { focusItem, activeVideo, voiceEditor, widgetDatas } = getState(),
            widget = activeVideo.frames[focusItem.path[1]],
            { languageCode, voice, template } = voiceEditor,
            { languageCode:prevLanguageCode, voice:prevVoice, template:prevTemplate, isSaving } = widget.voiceOver;

        if (isSaving) {
            dispatch(setVoiceEditor({...voiceEditor, status: IS_LOADING}));
        } else {
            if (prevTemplate !== template || prevVoice !== voice || prevLanguageCode !== languageCode) {
                let text = ActiveVideoUtils.parseVoice(template, widgetDatas[widget.id]);
                dispatch(setVoiceEditor({...voiceEditor, status: IS_LOADING}));
                axios.post(GET_SPEECH_URL, {template, text, voice, languageCode})
                    .then(({data}) => {
                        dispatch(setVoiceEditor({...data, status: IS_PLAYING}));
                    });
            } else {
                dispatch(setVoiceEditor({...voiceEditor, status: IS_PLAYING}));
            }
        }
    };
}