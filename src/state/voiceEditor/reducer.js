import initialState from './initialState'
import { SET_VOICE_EDITOR } from './actionTypes'

export default function reducer(state = initialState, {type, payload} = {}) {

    switch (type) {

        case SET_VOICE_EDITOR:
            return {...payload};

        default:
            return state

    }
}