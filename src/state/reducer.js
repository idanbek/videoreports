import { combineReducers } from 'redux'

import focusItem from './focusItem/reducer'
import widgetDatas from './widgetDatas/reducer'
import voiceEditor from './voiceEditor/reducer'
import dataSourcesReducer from './dataSources/reducer'
import activeVideoReducer from './activeVideo/reducer'

export default combineReducers({
    voiceEditor: voiceEditor,
    widgetDatas: widgetDatas,
    focusItem: focusItem,
    dataSources: dataSourcesReducer,
    activeVideo: activeVideoReducer
});