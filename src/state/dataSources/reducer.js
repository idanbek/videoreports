import initialState from './initialState.js'
import { ADD_ALL_SOURCES, ADD_SOURCE } from './actionTypes'

export default function reducer(state = initialState, {type, payload} = {}) {

    switch (type) {
        case ADD_SOURCE:
            state[payload.type] = {...state[payload.type], ...payload};
            return {...state};

        case ADD_ALL_SOURCES:
            return {...state, ...payload};

        default:
            return state
    }
}