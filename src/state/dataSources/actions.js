import axios from 'axios'
import getInitialStatus from '../shared/getInitialStatus'

import { ADD_ALL_SOURCES, ADD_SOURCE } from './actionTypes'
import { GET_INTEGRATIONS_URL } from '../../constants/urls'


export function addDataSource(payload) {
    return {
		type: ADD_SOURCE,
		payload: payload
	}
}

export function addAllDataSources(payload) {
    return {
		type: ADD_ALL_SOURCES,
		payload: payload
	}
}

export function fetchDataSources() {
    return (dispatch) => {
        axios.get(GET_INTEGRATIONS_URL)
            .then(({data}) => {
                data.status = getInitialStatus({isLoading: false});
                dispatch(addAllDataSources(data));
            });

    };
}

export function fetchMetrics(integrationType, postData={}) {
    return (dispatch, getState) => {
        axios.post(GET_INTEGRATIONS_URL + `${integrationType}/metrics`, postData)
            .then(({data}) => {
                let integration = getState().dataSources[integrationType] || {};
                integration['metrics'] = data.metrics;
                dispatch(addDataSource({...integration}));
            });
    };
}