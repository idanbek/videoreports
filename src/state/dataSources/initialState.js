import getInitialStatus from '../shared/getInitialStatus'

export default {
    status: getInitialStatus({isLoading: true})
}