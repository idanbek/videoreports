import initialState from './initialState'
import { SET_FOCUSED_ITEM } from './actionTypes'

export default function reducer(state = initialState, {type, payload} = {}) {

    switch (type) {
        case SET_FOCUSED_ITEM:
            return {...payload};

        default:
            return state
    }

}