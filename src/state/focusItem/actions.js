import swal from 'sweetalert'

import { SET_FOCUSED_ITEM } from './actionTypes.js'
import { setVoiceEditor } from '../voiceEditor/actions'
import { changeVoiceOver } from '../activeVideo/actions'

export function setFocus(payload) {
    return{
		type: SET_FOCUSED_ITEM,
		payload: payload
	}
}

export function setFocusItem(payload) {
    return (dispatch, getState) => {
        let { activeVideo:{ frames }, voiceEditor } = getState(),
            prevVoiceOver =  null;


        try{
            prevVoiceOver = frames[voiceEditor.widgetIndex].voiceOver
        }catch(e){

        }

        if (prevVoiceOver &&
            (prevVoiceOver.template !== voiceEditor.template ||
             prevVoiceOver.languageCode !== voiceEditor.languageCode ||
             prevVoiceOver.voice !== voiceEditor.voice)) {
            swal({
                title: 'Are you sure?',
                icon: 'warning',
                dangerMode: true,
                buttons: ['No', 'Yes'],
                text: 'You made some changes to the annotation of this metric, would you like to save those changes?',
                closeOnClickOutside: true,
                closeOnEsc: true
            }).then((yes) => {
                if (yes) {
                    let { languageCode, voice, template, widgetIndex, text } = voiceEditor;
                    if (prevVoiceOver.languageCode === languageCode && prevVoiceOver.voice === voice) {
                        dispatch(changeVoiceOver(
                            {prevVoiceOver, template, text, languageCode, voice},
                            ['frames', widgetIndex])
                        );
                    } else {
                        frames.forEach(({voiceOver}, idx) => {
                            let newTemplate = (idx === widgetIndex) ? template : voiceOver.template;
                            dispatch(changeVoiceOver(
                                {...voiceOver, template: newTemplate, languageCode, voice},
                                ['frames', idx])
                            );
                        });
                    }
                }
            });
        }

        if (payload.path instanceof Array && payload.path[0] === 'frames') {
            let widget = frames[payload.path[1]];
            dispatch(setVoiceEditor({
                ...widget.voiceOver,
                widgetIndex: payload.path[1]
            }))
        } else {
            dispatch(setVoiceEditor(null));
        }
        dispatch(setFocus(payload));
    }
}