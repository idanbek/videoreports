import { assign } from 'lodash'

export default function getInitialStatus(status) {
    return assign({
        isLoading: false,
    }, status);
}