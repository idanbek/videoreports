import React from 'react'


class SaveOptionsInput extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            textValue: ''
        }
    }

    updateInputValue(evt) {
        this.setState({
            textValue: evt.target.value
        })
    }

    handleClick() {

        if(this.inputElement.validity.valid) {

            let {textValue} = this.state,
                {onClicked} = this.props;

            onClicked(textValue)
        }
    }


    render() {
        let { textValue } = this.state,
            { placeholder } = this.props;


        return (
            <div className="save-options-container">
                <form className="save-options" action="javascript:myFunction(); return false;" >
                    <input value={ textValue }
                           placeholder={ placeholder }
                           type="text"
                           required
                           pattern='.+'
                           onChange={ this.updateInputValue.bind(this) }
                           ref={ (element)=>{ this.inputElement = element } }
                    />
                    <button
                        type={'submit'}
                        className="btn"
                        onClick={ this.handleClick.bind(this) }>
                        Ok
                    </button>
                </form>
            </div>
        );
    }
}

export default SaveOptionsInput;