import React, {Component} from 'react'
import Select from 'react-select'
import {GET_ALL_SLACK_CHANNELS} from "../constants/urls";



export default class SlackChannelSelector extends Component {
    static DEFAULT_USER_IMAGE = 'https://secure.gravatar.com/avatar/2f5383bfdd4e85b83c420a18b7d927bb.jpg?s=48&d=https%3A%2F%2Fa.slack-edge.com%2F0180%2Fimg%2Favatars%2Fava_0001-48.png';

    constructor(props){
        super(props);
        this.state = {
            value:null
        }
    }

    render() {
        let { value } = this.state,
            { onChange } = this.props;


         return <Select.Async
                        multi
                      name="form-field-name"
                      labelKey='name'
                      className='slack-channel-selector'
                      valueKey='id'
                      value={ value }
                      optionRenderer={
                          (option)=>
                              <div className='slack-channel-option'><img src={option.image || SlackChannelSelector.DEFAULT_USER_IMAGE} alt={''}/><div className='title'>{option.name}</div></div>
                      }

                      onChange={
                          (value)=> {
                              this.setState({value})
                              onChange(value)
                          }
                      }
                      loadOptions={ ()=>fetch(GET_ALL_SLACK_CHANNELS)
                              .then(response=>response.json())
                              .then(options=>{
                                  return {options}
                                }
                              )}
                    />

    }
}

