import React from 'react'
import moment from 'moment'
import { Button } from 'react-bootstrap'
import axios from 'axios'

import { MODIFY_VIDEO_URL , DELETE_VIDEO_URL } from "../constants/urls";
import SimpleWidget from '../routes/CreateReport/components/Report/components/SimpleWidget.react'
import { getVideoHeight } from "../utils/getVideoHeight";


export default class UserVideo extends React.Component {

    constructor(props) {
        super(props);
        let { draft: { name } } = props;
        this.state = {
            videoName: name
        }
    }

    handlePublish() {
        let { onPublish , draft } = this.props ,
            { videoName } = this.state;

        onPublish({ ...draft , name: videoName })
    }

    handleCustomize() {
        let { onCustomize , draft } = this.props ,
            { videoName } = this.state;

        onCustomize({ ...draft , name: videoName })
    }

    componentWillUnmount() {
        let { videoName , deleted } = this.state ,
            { draft , draft: { name } , videoID: id } = this.props;

        // Delete video from DB.
        if (deleted) {
            axios.post(DELETE_VIDEO_URL , { id }).then(response => {
                console.log(response)
            })
        }
        // Video name was changed, we need to update DB.
        else if (name !== videoName) {
            axios.post(MODIFY_VIDEO_URL , { video_json: { ...draft , name: videoName } , id }).then(response => {
                console.log(response)
            })

        }

    }


    render() {
        let { draft , date } = this.props ,
            { bg } = draft ,
            { videoName , deleted } = this.state;


        return deleted ? null : (
            <div className='UserVideoContainer'>
                <div className='UserVideoWrapper'>
                    <div className='ClickGetter'>
                        <div className='date-view'>{moment.unix(date).format("MMM Do YY")}</div>

                        <div className='UserVideoBtnWrapper'>
                            <Button onClick={this.handlePublish.bind(this)}>Publish</Button>

                            <Button onClick={this.handleCustomize.bind(this)}>Customize</Button>
                        </div>
                    </div>

                    <div className="frames-wrapper"
                         style={{backgroundImage: `url(${bg.backgroundImage})${bg.backgroundGradient? `,${bg.backgroundGradient}`:''}`, height: getVideoHeight(draft.frames) }}>
                        {draft.frames
                            .filter(widget => widget.widgetType !== 'EMPTY')
                            .map((widget , idx) =>
                                <SimpleWidget
                                    style={
                                        {
                                            position: 'absolute' ,
                                            transform: `matrix(1,0,0,1,${widget.position.x * 480},${widget.position.y * 270})`
                                        }
                                    }
                                    key={idx}
                                    setIn={() => {
                                    }}
                                    focusPath={[]}
                                    widgetPath={[ 'frames' , idx ]}
                                    themeColors={bg.themeColors}
                                    widget={widget}/>
                            )
                        }
                    </div>
                </div>


                <div className='TitleContainer'>
                    <input className='title' value={videoName} onChange={
                        (event) => {
                            this.setState({ videoName: event.target.value })

                        }
                    }/>

                    <div className='delete' onClick={() => {
                        this.setState({ deleted: true })
                    }}>
                        Delete
                        <i className="fa fa-trash-o" aria-hidden="true"/>
                    </div>
                </div>
            </div>
        );
    }
}

