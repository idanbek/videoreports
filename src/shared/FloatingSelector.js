import React from 'react'


export default class FloatingSelector extends React.Component {

    // handleClick(option){
    //     let { onPicked } = this.props;
    //     onPicked(option);
    // }

    render() {
        let { options, selected, id, onPicked } = this.props;

        return (
            <div className="FloatingSelectorWrapper" id={ id }>
                { options.map((option, idx)=>
                    <div key={ idx }
                         className={ `FloatingOption ${option.value === selected? 'active' : ''}` }
                         onClick={ onPicked.bind(this, option) }>
                        { option.label }
                        { selected === option.value && <i className="fa fa-check" aria-hidden="true"/> }
                    </div>
                )}
            </div>
        );
    }
}

