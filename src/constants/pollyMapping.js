import map from 'lodash/map'

export const POLLY_MAPPING = {
    'da-DK': {name: 'Danish', voices: ['Mads', 'Naja']},
    'nl-NL': {name: 'Dutch', voices: ['Lotte', 'Ruben']},
    'en-AU': {name: 'English (Australian)', voices: ['Nicole', 'Russel']},
    'en-GB': {name: 'English (British)', voices: ['Amy', 'Brian', 'Emma']},
    'en-US': {name: 'English (US)', voices: ['Ivy', 'Joanna', 'Joey', 'Justin', 'Kendra', 'Kimberly', 'Salli']},
    'fr-FR': {name: 'French', voices: ['Celine', 'Mathieu']},
    'fr-CA': {name: 'French (Canadian)', voices: ['Chantal']},
    'de-DE': {name: 'German', voices: ['Hans', 'Marlene', 'Vicky']},
    'is-IS': {name: 'Icelandic', voices: ['Dora', 'Karl']},
    'it-IT': {name: 'Italian', voices: ['Carla', 'Giorgio']},
    'ja-JP': {name: 'Japanese', voices: ['Mizuki']},
    'nb-NO': {name: 'Norwegian', voices: ['Liv']},
    'pl-PL': {name: 'Polish', voices: ['Jacek', 'Jan', 'Ewa', 'Maja']},
    'pt-BR': {name: 'Portuguese (Brazilian)', voices: ['Ricardo', 'Vitoria']},
    'pt-PT': {name: 'Portuguese (European)', voices: ['Cristiano', 'Ines']},
    'ro-RO': {name: 'Romanian', voices: ['Carmen']},
    'ru-RU': {name: 'Russian', voices: ['Maxim', 'Tatyana']},
    'es-ES': {name: 'Spanish (Castilian)', voices: ['Conchita', 'Enrique']},
    'es-US': {name: 'Spanish (Latin American)', voices: ['Miguel', 'Penelope']},
    'sv-SE': {name: 'Swedish', voices: ['Astrid']},
    'tr-TR': {name: 'Turkish', voices: ['Fliz']},
    'cy-GB': {name: 'Welsh', voices: ['Gwyneth']}
};
export const LANGUAGES = map(POLLY_MAPPING, (value, key)=> {return {label: value.name, value: key, voices: value.voices}});

export const IS_PLAYING = 'IS_PLAYING';

export const IS_LOADING = 'IS_LOADING';

export const IS_PAUSED = 'IS_PAUSED';

export const DEFAULT_CODE = 'en-GB';

export const DEFAULT_VOICE = 'Brian';