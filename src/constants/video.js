export const NUM_OF_ROWS = 4;
export const NUM_OF_COLUMNS = 4;
export const BLOCK_WIDTH = 1920 / NUM_OF_COLUMNS;
export const BLOCK_HEIGHT = 1080 / NUM_OF_ROWS;