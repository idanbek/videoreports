export const FB_SDK = window.FB;

export const FB_CONFIG = {
    appId: '1007349092734783',
    status: true,
    cookie: true,
    xfbml: true,
    version: 'v2.10'
};

export const FB_PERMISSIONS = 'ads_management';