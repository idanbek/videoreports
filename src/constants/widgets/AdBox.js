export const DEFAULT_FACEBOOK_FIELDS = [
    {label: 'Impressions', value: 'impressions'},
    {label: 'Clicks', value: 'clicks'},
    {label: 'PPC', value: 'ppc'},
];

export default {
        name: 'Ad Box',
        widgetType: 'SIMPLE',
        voiceOver:{
            languageCode:'en-GB',
            voice:'Amy',
            src: 'https://s3.amazonaws.com/static.video-reports.text-to-speech/bc60cb94-5221-46b1-94f4-fadf4a13fde6.mp3',
            text: 'This is the facebook ad. #YOLO.',
            template: 'This is the facebook ad. #YOLO.',
            length: '3.16'
        },
        version: 0,
        size: {height: 3, width: 2},
        position: {x: 0, y: 0},
        decorators: [
            // image decorator not change position must be first
            {
                type: 'div',
                className: 'decAdBoxImage',
                style: {backgroundImage: ''}
            },
            // Wrapper
            {
                type: 'div',
                className: 'decAdBoxWrapper',
                style: {}
            },

        ],
        labels: [
            // Data Labels (do not edit positions Sergey)
            // Reach
            {
                text: DEFAULT_FACEBOOK_FIELDS[0].label,
                themeStyle: {color: 2},
                style: {
                    height: '35px',
                    fontSize: '38px',
                    lineHeight: '35px',
                    textAlign: 'left',
                    transform: 'matrix(1, 0, 0, 1, 590, 200)',
                    textDecoration: 'underline',
                    zIndex: 3,
                    fontFamily: 'Montserrat',
                    width: '240px',
                }
            },
            {
                text: '0',
                style: {
                    height: '55px',
                    fontSize: '44px',
                    lineHeight: '65px',
                    fontFamily: 'Kalam',
                    fontWeight: 'bold',
                    textAlign: 'center',
                    color: 'black',
                    transform: 'matrix(1, 0, 0, 1, 590, 245)',
                    zIndex: 3,
                    width: '240px',
                }
            },

            // Clicks
            {
                text: DEFAULT_FACEBOOK_FIELDS[1].label,
                 themeStyle: {color: 2},
                style: {
                    height: '35px',
                    fontSize: '38px',
                    lineHeight: '35px',
                    textAlign: 'center',
                        transform: 'matrix(1, 0, 0, 1, 590, 320)',
                     textDecoration: 'underline',
                    zIndex: 3,
                    fontFamily: 'Montserrat',
                    width: '240px',
                }
            },
            {
                text: '0',
                style: {
                    height: '55px',
                    fontSize: '44px',
                    lineHeight: '65px',
                    fontFamily: 'Kalam',
                    fontWeight: 'bold',
                    textAlign: 'center',
                    color: 'black',
                    transform: 'matrix(1, 0, 0, 1,590, 365)',
                    zIndex: 3,
                    width: '240px',
                }
            },
            //Cost Per Click
            {
                text: DEFAULT_FACEBOOK_FIELDS[2].label,
                 maxFontSize:43,
                 themeStyle: {color: 2},
                style: {
                    height: '35px',
                    fontSize: '38px',
                    lineHeight: '35px',
                    textAlign: 'center',
                    transform: 'matrix(1, 0, 0, 1, 590, 440)',
                    zIndex: 3,
                    fontFamily: 'Montserrat',
                    textDecoration: 'underline',
                    width: '240px',
                }
            },
            {
                text: '0',
                style: {
                    height: '55px',
                    fontSize: '44px',
                    lineHeight: '65px',
                    fontFamily: 'Kalam',
                    fontWeight: 'bold',
                    textAlign: 'center',
                    color: 'black',
                    transform: 'matrix(1, 0, 0, 1, 590, 485)',
                    zIndex: 3,
                    width: '240px',
                }
            },
            //End Data Labels
        ],
        images: []
    };