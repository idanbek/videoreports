import { WIDGET_ICONS_URL } from '../urls'

export const DEFAULT_FACEBOOK_FIELDS = [
    {label: 'Impressions', value: 'impressions'},
    {label: 'Clicks', value: 'clicks'},
    {label: 'CPM', value: 'cpm'},
];

export default {
        name: 'Triple Metric',
        voiceOver:{
            languageCode: 'en-GB',
            voice: 'Amy',
            template: 'This is the triple metric. Wow lots of stuff.',
            text: 'This is the triple metric. Wow lots of stuff.',
            src: 'https://s3.amazonaws.com/static.video-reports.text-to-speech/e1ac2f9b-31c5-4ca7-96ff-87d927814068.mp3',
            length: '3.16'
        },
        widgetType: 'SIMPLE',
        version: 0,
        size: {height: 2, width: 1},
        position: {x: 0, y: 0},
        decorators: [],
        labels: [
            // data labels (not change positions Sergey)
            {
                text: DEFAULT_FACEBOOK_FIELDS[0].label,
                maxFontSize:45,
                style: {
                    height: '35px',
                    fontSize: '34px',
                    lineHeight: '35px',
                    textAlign: 'left',
                    transform: 'matrix(1, 0, 0, 1, 160, 125)',
                    zIndex: 3,
                    fontFamily: 'Montserrat',
                    width: '310px',
                }
            },
            {
                text: '0',
                maxFontSize:60,
                style: {
                    height: '45px',
                    fontSize: '48px',
                    lineHeight: '45px',
                    fontFamily: 'Montserrat',
                    fontWeight: '600',
                    textAlign: 'left',
                    transform: 'matrix(1, 0, 0, 1, 160, 185)',
                    zIndex: 3,
                    width: '310px',
                }
            },
            {
                text: DEFAULT_FACEBOOK_FIELDS[1].label,
                maxFontSize:45,
                style: {
                    height: '35px',
                    fontSize: '34px',
                    lineHeight: '35px',
                    textAlign: 'left',
                    transform: 'matrix(1, 0, 0, 1, 160, 265)',
                    zIndex: 3,
                    fontFamily: 'Montserrat',
                    width: '310px',
                }
            },
            {
                text: '0',
                maxFontSize:60,
                style: {
                    height: '45px',
                    fontSize: '48px',
                    lineHeight: '45px',
                    fontFamily: 'Montserrat',
                    fontWeight: '600',
                    textAlign: 'left',
                    transform: 'matrix(1, 0, 0, 1, 160, 325)',
                    zIndex: 3,
                    width: '310px',
                }
            }, {
                text: DEFAULT_FACEBOOK_FIELDS[2].label,
                maxFontSize:45,
                style: {
                    height: '35px',
                    fontSize: '34px',
                    lineHeight: '35px',
                    textAlign: 'left',
                    transform: 'matrix(1, 0, 0, 1, 160, 405)',

                    zIndex: 3,
                    fontFamily: 'Montserrat',
                    width: '310px',
                }
            },
            {
                text: '0',
                maxFontSize:60,
                style: {
                    height: '45px',
                    fontSize: '48px',
                    lineHeight: '45px',
                    fontFamily: 'Montserrat',
                    fontWeight: '600',
                    textAlign: 'left',

                    transform: 'matrix(1, 0, 0, 1, 160, 465)',
                    zIndex: 3,
                    width: '310px',
                }
            },
            // end data labels (do not change positions Sergey)
            {
                text: 'Costs',
                themeStyle: {background: 0},
                style: {
                    transform: 'matrix(1, 0, 0, 1, 70, 5)',
                    zIndex: 2,
                    color: 'white',
                     boxShadow:'rgba(0, 0, 0, 0.75) 0px 0px 14px 0px',
                    fontFamily: 'Kalam',
                    fontSize: '50px',
                    textAlign: 'center',
                    height: '55px',
                    lineHeight: '65px',
                    width: '290px',
                }
            }
        ],
        images: [
            {
                style: {
                    transform: 'matrix(1, 0, 0, 1, 48, 140)',

                    height: '65px',
                    zIndex: 3,
                    width: '65px',
                },
                iconID:'speech_bubble',
                src: `${WIDGET_ICONS_URL}speech_bubble.svg`,
                coordinates: {zoom: 1, x: 0, y: 0}
            },
            {
                style: {
                    transform: 'matrix(1, 0, 0, 1, 48, 280)',
                    height: '65px',
                    zIndex: 3,
                    width: '65px',
                },
                iconID:'cursor',
                src: `${WIDGET_ICONS_URL}cursor.svg`,
                coordinates: {zoom: 1, x: 0, y: 0}
            },
            {
                style: {
                    transform: 'matrix(1, 0, 0, 1, 48, 420)',

                    height: '65px',
                    zIndex: 3,
                    width: '65px',
                },
                iconID: 'target',
                src: `${WIDGET_ICONS_URL}target.svg`,
                coordinates: {zoom: 1, x: 0, y: 0}
            }
        ]
    };