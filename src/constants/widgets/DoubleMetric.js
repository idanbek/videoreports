import { WIDGET_ICONS_URL} from '../urls'

export const DEFAULT_FACEBOOK_FIELDS = [
    {label: 'Impressions', value: 'impressions'},
    {label: 'Clicks', value: 'clicks'},
    {label: 'Cost Per Unique Click', value: 'cost_per_unique_click'},
];

export default {
    name: 'Double Metric',
    voiceOver:{
        languageCode: 'en-GB',
        voice: 'Amy',
        src: 'https://s3.amazonaws.com/static.video-reports.text-to-speech/64d4f15f-071e-41e0-aad8-dc186e0ef09b.mp3',
        template: 'This is the double metric. It is a little bit longer.',
        text: 'This is the double metric. It is a little bit longer.',
        length: '3.55'
    },
    widgetType: 'SIMPLE',
    version: 0,
    size: {height: 1, width: 2},
    position: {x: 0, y: 0},
    decorators: [
        // Wrapper
        {
            type: 'div',
            className: 'decDoubleMetric',
            style: {}
        },
    ],
    labels: [
        {
            text: DEFAULT_FACEBOOK_FIELDS[0].label,
            style: {
                height: '35px',
                fontSize: '48px',
                lineHeight: '35px',
                textAlign: 'left',
                transform: 'matrix(1, 0, 0, 1, 90, 87)',
                zIndex: 3,
                fontFamily: 'Kalam',
                fontWeight:'bold',
                width: '350px',
            }
        },
        {
            text: '0',
             themeStyle: {color: 2},
            style:
                {
                    height: '37px',
                    fontSize: '45px',
                    lineHeight: '37px',
                    fontFamily: 'Montserrat',
                    fontWeight:'500',
                    textAlign: 'left',
                    transform: 'matrix(1, 0, 0, 1, 90, 147)',
                    zIndex: 3,
                    width: '350px',
                }
        },
        {
            text: DEFAULT_FACEBOOK_FIELDS[1].label,
            style: {
                height: '35px',
                fontSize: '30px',
                lineHeight: '35px',
                textAlign: 'left',
                transform: 'matrix(1, 0, 0, 1, 593, 87)',
                zIndex: 3,
                 fontFamily: 'Montserrat',
                fontWeight:'500',
                width: '340px',
            }
        },
        {
            text: '0',
            themeStyle: {color: 2},
            style: {
                height: '37px',
                fontSize: '45px',
                lineHeight: '37px',
                fontFamily: 'Montserrat',
                fontWeight:'500',
                textAlign: 'left',
                transform: 'matrix(1, 0, 0, 1, 593, 147)',
                zIndex: 3,
                width: '340px',
            }
        },
        // end data labels
        {
            text: 'Overview',
            themeStyle: {background: 1},
            style: {
                transform: 'matrix(1, 0, 0, 1, 284, 7)',
                boxShadow:'rgba(0, 0, 0, .55) 0px 0px 13px 0px',
                zIndex: 2,
                color: 'white',
                fontFamily: 'Kalam',
                fontSize: '53px',
                textAlign: 'center',
                height: '63px',
                lineHeight:'63px',
                width: '309px',
            }
        }
    ],
    images: [
        {
            style: {
                transform: 'matrix(1, 0, 0, 1, 10, 87)',
                height: '70px',
                zIndex: 3,
                width: '70px',
            },
            iconID:'finances',
            src: `${WIDGET_ICONS_URL}finances.svg`,
            coordinates: {zoom: 1, x: 0, y: 0}
        },
        {
            style: {
                transform: 'matrix(1, 0, 0, 1, 500, 87)',
                height: '70px',
                zIndex: 3,
                width: '70px',
            },
            iconID:'http',
            src: `${WIDGET_ICONS_URL}http.svg`,
            coordinates: {zoom: 1, x: 0, y: 0}
        }
    ]
};