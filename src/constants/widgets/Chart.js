export default {
    name: 'Chart',
    voiceOver: {
        languageCode: 'en-GB',
        voice: 'Amy',
        length: '1.20',
        template: 'Lets move on to the next subject',
        text: 'Alot of data here...',
        src: 'https://s3.amazonaws.com/static.video-reports.text-to-speech/3ce0bb0a-cbaa-4fca-aaf1-63bfde7b2133.mp3'
    },
    widgetType: 'SIMPLE',
    version: 0,
    size: {height: 2, width: 2},
    position: {x: 0, y: 0},
    decorators: [
        // Wrapper
        {
            type: 'div',
            className: 'decChartWrapper',
            style: {}
        },

       ],
    labels: [

        {
                text: 'Graph',
                maxFontSize:50,
                themeStyle: {background: 0},
                style: {
                    transform: 'matrix(1, 0, 0, 1, 331, 10)',
                    boxShadow:'0px 0px 21px 0px rgba(0,0,0,0.75)',
                    zIndex: 2,
                    color: 'white',
                    fontFamily: 'Kalam',
                    fontSize: '45px',
                    textAlign: 'center',
                    lineHeight:'54px',
                    height: '54px',
                    width: '326px',
                }
            }

            ],
    images: [],
    graph: {
        style: {height: '470px', width: '750px', transform: 'matrix(1,0,0,1,80,10)'},
    },
};