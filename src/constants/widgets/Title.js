import { BASE_URL} from '../urls'

export default {
        name: 'Title',
        voiceOver:{
            languageCode: 'en-GB',
            voice: 'Amy',
            text: 'Lets move on to the next subject',
            template: "Lets move on to the next subject",
            src: 'https://s3.amazonaws.com/static.video-reports.text-to-speech/377b4ffd-7f2d-461f-9620-55da7108d989.mp3',
            length: '1.96'
        },
        widgetType: 'SIMPLE',
        version: 0,
        size: {height: 1, width: 4},
        position: {x: 0, y: 0},
        decorators: [

        ],
        labels: [
            {
                 maxFontSize:59,
                themeStyle: {background: 2},
                text: 'BEST TARGETING ADS',
                style: {
                    transform: 'matrix(1,0,0,1,567,99)',
                    boxShadow:'rgba(0, 0, 0, 0.75) 0px 0px 10px 0px',
                    color: 'white',
                    fontFamily: 'Kalam',
                    lineHeight:'70px',
                    fontWeight:'bold',
                    fontSize: '52px',
                    zIndex: 2,
                    overflow: 'hidden',
                    textAlign: 'center',
                    height: '65px',
                    width: '796px',
                }
            }
        ],
        images: []
    };