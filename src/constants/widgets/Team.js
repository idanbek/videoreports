import { DEFAULT_IMAGES_URL} from '../urls'

export default {
    name: 'Team' ,
    widgetType: 'SIMPLE' ,
    voiceOver: {
        languageCode: 'en-GB' ,
        voice: 'Amy' ,
        src: 'https://s3.amazonaws.com/static.video-reports.text-to-speech/52e09666-009b-4d65-a1ab-da2ae8ca90f4.mp3' ,
        length: '1.49' ,
        template: 'This is the single metric.' ,
        text: 'This is the single metric.'
    } ,
    version: 0 ,
    size: { height: 2 , width: 4 } ,
    position: { x: 0 , y: 0 } ,
    decorators: [] ,
    labels: [
        {
            text: 'Alex-CEO' ,
            style: {
                height: '45px' ,
                fontSize: '40px' ,
                lineHeight: '45px' ,
                textAlign: 'center' ,
                transform: 'matrix(1, 0, 0, 1, 585, 220)' ,
                zIndex: 3 ,
                fontFamily: 'Permanent Marker' ,
                width: '250px' ,
            }
        },
        {
            text: 'Avi-COO' ,
            style: {
                height: '45px' ,
                fontSize: '40px' ,
                lineHeight: '45px' ,
                textAlign: 'center' ,
                transform: 'matrix(1, 0, 0, 1, 1115, 220)' ,
                zIndex: 3 ,
                fontFamily: 'Permanent Marker' ,
                width: '250px' ,
            }
        },

        {
            text: 'Sergey- PPC expert' ,
            style: {
                height: '45px' ,
                fontSize: '40px' ,
                lineHeight: '45px' ,
                textAlign: 'center' ,
                transform: 'matrix(1, 0, 0, 1, 67, 495)' ,
                zIndex: 3 ,
                fontFamily: 'Permanent Marker' ,
                width: '250px' ,
            }
        },
        {
            text: 'Idan- SEO ninja' ,
            style: {
                height: '45px' ,
                fontSize: '40px' ,
                lineHeight: '45px' ,
                textAlign: 'center' ,
                transform: 'matrix(1, 0, 0, 1, 451, 495)' ,
                zIndex: 3 ,
                fontFamily: 'Permanent Marker' ,
                width: '250px' ,
            }
        },
        {
            text: 'Yaniv- Facebook master' ,
            style: {
                height: '45px' ,
                fontSize: '40px' ,
                lineHeight: '45px' ,
                textAlign: 'center' ,
                transform: 'matrix(1, 0, 0, 1, 841, 495)' ,
                zIndex: 3 ,
                fontFamily: 'Permanent Marker' ,
                width: '250px' ,
            }
        },
        {
            text: 'Igor- Designer' ,
            style: {
                height: '45px' ,
                fontSize: '40px' ,
                lineHeight: '45px' ,
                textAlign: 'center' ,
                transform: 'matrix(1, 0, 0, 1, 1219, 495)' ,
                zIndex: 3 ,
                fontFamily: 'Permanent Marker' ,
                width: '250px' ,
            }
        },
        {
            text: 'Ivgeni- Data scientist' ,
            style: {
                height: '45px' ,
                fontSize: '40px' ,
                lineHeight: '45px' ,
                textAlign: 'center' ,
                transform: 'matrix(1, 0, 0, 1, 1603, 495)' ,
                zIndex: 3 ,
                fontFamily: 'Permanent Marker' ,
                width: '250px' ,
            }
        }

    ] ,
    images: [
        {
            style: {
                transform: 'matrix(1,0,0,1,605,10)' ,
                height: '210px' ,
                zIndex: 3 ,
                borderRadius: '100%' ,
                width: '210px' ,
            } ,
            src: `${DEFAULT_IMAGES_URL}profile.jpg` ,
            coordinates: { zoom: .8 , x: -43 , y: -32 }
        } ,
        {
            style: {
                transform: 'matrix(1,0,0,1,1135,10)' ,
                height: '210px' ,
                zIndex: 3 ,
                borderRadius: '100%' ,
                width: '210px' ,
            } ,
           src: `${DEFAULT_IMAGES_URL}profile.jpg` ,
            coordinates: { zoom: .8 , x: -43 , y: -32 }
        } ,
        {
            style: {
                transform: 'matrix(1,0,0,1,87,285)' ,
                height: '210px' ,
                zIndex: 3 ,
                borderRadius: '100%' ,
                width: '210px' ,
            } ,
            src: `${DEFAULT_IMAGES_URL}profile.jpg` ,
            coordinates: { zoom: .8 , x: -43 , y: -32 }
        } ,

         {
            style: {
                transform: 'matrix(1,0,0,1,471,285)' ,
                height: '210px' ,
                zIndex: 3 ,
                borderRadius: '100%' ,
                width: '210px' ,
            } ,
           src: `${DEFAULT_IMAGES_URL}profile.jpg` ,
            coordinates: { zoom: .8 , x: -43 , y: -32 }
        } ,

        {
            style: {
                transform: 'matrix(1,0,0,1,861,285)' ,
                height: '210px' ,
                zIndex: 3 ,
                borderRadius: '100%' ,
                width: '210px' ,
            } ,
            src: `${DEFAULT_IMAGES_URL}profile.jpg` ,
            coordinates: { zoom: .8 , x: -43 , y: -32 }
        } ,
        {
            style: {
                transform: 'matrix(1,0,0,1,1239,285)' ,
                height: '210px' ,
                zIndex: 3 ,
                borderRadius: '100%' ,
                width: '210px' ,
            } ,
           src: `${DEFAULT_IMAGES_URL}profile.jpg` ,
            coordinates: { zoom: .8 , x: -43 , y: -32 }
        },
        {
            style: {
                transform: 'matrix(1,0,0,1,1623,285)' ,
                height: '210px' ,
                zIndex: 3 ,
                borderRadius: '100%' ,
                width: '210px' ,
            } ,
           src: `${DEFAULT_IMAGES_URL}profile.jpg` ,
            coordinates: { zoom: .8 , x: -43 , y: -32 }
        }


    ]
};