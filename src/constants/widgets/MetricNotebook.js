import { WIDGET_ICONS_URL} from '../urls'

export const DEFAULT_FACEBOOK_FIELDS = [
    {label: 'Impressions', value: 'impressions'},
    {label: 'Clicks', value: 'clicks'},
    {label: 'Cost Per Unique Click', value: 'cost_per_unique_click'},
];

export default {
        name: 'Metric Notebook',
        widgetType: 'SIMPLE',
        voiceOver:{
            languageCode:'en-GB',
            voice:'Amy',
            src: 'https://s3.amazonaws.com/static.video-reports.text-to-speech/bc60cb94-5221-46b1-94f4-fadf4a13fde6.mp3',
            text: 'This is the facebook ad. #YOLO.',
            template: 'This is the facebook ad. #YOLO.',
            length: '3.16'
        },
        version: 0,
        size: {height: 2, width: 2},
        position: {x: 0, y: 0},
        decorators: [
            {
                type: 'div',
                className: 'decMetricNotebook',
                style: {},
            },
        ],
        labels: [
            // data labels (not change positions Sergey)
            {
                text: DEFAULT_FACEBOOK_FIELDS[0].label,
                style: {
                    color: 'black',
                    height: '35px',
                    fontSize: '40px',
                    lineHeight: '35px',
                    textAlign: 'left',
                    transform: 'matrix(1, 0, 0, 1, 220, 148) rotate(-2deg)',
                    zIndex: 3,
                    fontFamily: 'Montserrat',
                    fontWeight:'600',
                    width: '510px',
                }
            },
            {
                text: '0',
                maxFontSize:45,
                themeStyle: {color: 2},
                style: {
                    height: '45px',
                    fontSize: '53px',
                    lineHeight: '57px',
                    fontFamily: 'Kalam',
                    fontWeight: 'bold',
                    textAlign: 'left',
                    transform: 'matrix(1, 0, 0, 1, 220, 198) rotate(-2deg)',
                    zIndex: 3,
                    width: '510px',
                }
            },
            {
                text: DEFAULT_FACEBOOK_FIELDS[1].label,
                style: {
                    color: 'black',
                    height: '35px',
                    fontSize: '40px',
                    lineHeight: '35px',
                    textAlign: 'left',
                    transform: 'matrix(1, 0, 0, 1, 220, 270) rotate(-2deg)',
                    zIndex: 3,
                     fontFamily: 'Montserrat',
                    fontWeight:'600',
                    width: '510px',
                }
            },
            {
                text: '0',
                     themeStyle: {color: 2},
                 maxFontSize:45,
                style: {
                    height: '45px',
                    fontSize: '53px',
                    lineHeight: '57px',
                    fontFamily: 'Kalam',
                    fontWeight: 'bold',
                    textAlign: 'left',
                    transform: 'matrix(1, 0, 0, 1, 220, 320) rotate(-2deg)',
                    zIndex: 3,
                    width: '510px',
                }
            },
            {
                text: DEFAULT_FACEBOOK_FIELDS[2].label,
                maxFontSize:45,
                style: {
                    height: '35px',
                    fontSize: '40px',
                    lineHeight: '35px',
                    textAlign: 'left',
                    transform: 'matrix(1, 0, 0, 1, 220, 395) rotate(-2deg)',
                    zIndex: 3,
                    color: 'black',
                     fontFamily: 'Montserrat',
                    fontWeight:'600',
                    width: '510px',
                }
            },
            {
                text: '0',
                maxFontSize:45,
                     themeStyle: {color: 2},
                style: {
                    height: '45px',
                    fontSize: '53px',
                    lineHeight: '57px',
                    fontFamily: 'Kalam',
                    fontWeight: 'bold',
                    textAlign: 'left',
                    transform: 'matrix(1, 0, 0, 1, 220, 439) rotate(-2deg)',
                    zIndex: 3,
                    width: '510px',
                }
            },
            // end data labels
            {
                text: 'Engagements',
                themeStyle: {background: 0},
                style: {
                    transform: 'matrix(1, 0, 0, 1, 171, 55) rotate(-.5deg)',
                     boxShadow:'rgba(0, 0, 0, .55) 0px 0px 13px 0px',
                    zIndex: 2,
                    color: 'white',
                    fontFamily: 'Kalam',
                    fontSize: '52px',
                    lineHeight:'70px',
                    textAlign: 'center',
                    height: '68px',
                    width: '349px',
                }
            }
        ],
        images: [
            {
                style: {
                    transform: 'matrix(1, 0, 0, 1, 110, 166) rotate(-2deg)',
                    height: '70px',
                    zIndex: 3,
                    width: '70px',
                },
                iconID:'funnel',
                src: `${WIDGET_ICONS_URL}funnel.svg`,
                coordinates: {zoom: 1, x:0, y: 0}
            },
            {
                style: {
                    transform: 'matrix(1, 0, 0, 1, 110, 287) rotate(-2deg)',
                    height: '70px',
                    zIndex: 3,
                    width: '70px',
                },
                iconID:'cursor',
                src: `${WIDGET_ICONS_URL}cursor.svg`,
                coordinates: {zoom: 1, x: 0, y: 0}
            },
            {
                style: {
                    transform: 'matrix(1, 0, 0, 1, 110, 415) rotate(-2deg)',
                    height: '70px',
                    zIndex: 3,
                    width: '70px',
                },
                iconID:'graphic',
                src: `${WIDGET_ICONS_URL}graphic.svg`,
                coordinates: {zoom: 1, x: 0, y: 0}
            }
        ]
    };