import { DEFAULT_IMAGES_URL} from '../urls'

export default {
        name: 'Main title',
        voiceOver:{
            languageCode: 'en-GB',
            voice: 'Amy',
            length: '2.77',
            template: 'Hello alex this welcome to your weekly report',
            text: 'Hello alex this welcome to your weekly report',
            src: 'https://s3.amazonaws.com/static.video-reports.text-to-speech/10ac036d-84dc-4f4b-b382-4904aeb84383.mp3'
        },
        widgetType: 'SIMPLE',
        staticWidget: true,
        version: 0,
        size: {height: 1, width: 4},
        position: {x: 0, y: 0},
        decorators: [],
        labels: [
            {
                text: 'Your weekly Facebook ads report',
                themeStyle: {background: 0},
                maxFontSize:76,
                style: {
                    transform: 'matrix(1, 0, 0, 1, 368, 58) rotate(-0.5deg)',
                    color: 'white',
                    fontFamily: 'Montserrat',
                    fontSize: '65px',
                    textAlign: 'center',
                    fontWeight:'600',
                    height: '76px',
                    lineHeight:'76px',
                    width: '1164px',
                }
            },
            {
                text: 'FOR LAST WEEK',
                maxFontSize:70,
                themeStyle: {background: 1},
                style: {
                    transform: 'matrix(1, 0, 0, 1, 482, 157) rotate(-0.5deg)',
                    fontFamily: 'Kalam',
                    lineHeight:'100px',
                    fontWeight:'bold',
                    color: 'white',
                    fontSize: '65px',
                    textAlign: 'center',
                    height: '79px',
                    width: '559px',
                }
            },
            {
                text: '11.4-17.4',
                maxFontSize:80,
                timePicker: true,
                themeStyle:{color:2},
                style: {
                    transform: 'matrix(1, 0, 0, 1, 1088, 157)',
                    color: 'rgb(162, 23, 128)',
                    fontSize: '77px',
                    lineHeight:'79px',
                    fontWeight:'800',
                    fontFamily: 'Montserrat',
                    textAlign: 'center',
                    height: '79px',
                    width: '390px',

                }
            }
        ],
        images: [
            {
                style: {
                    transform: 'matrix(1, 0, 0, 1, 1599, 59)',
                    height: '154px',
                    width: '278px',
                },
                src: `${DEFAULT_IMAGES_URL}customer-logo.jpg`,
                coordinates: {zoom: 1.5, x: 50, y: 27}
            },
            {
                style: {
                    transform: 'matrix(1, 0, 0, 1, 67, 59)',
                    height: '154px',

                    width: '278px',
                },
                src: `${DEFAULT_IMAGES_URL}your-logo.jpg`,
                coordinates: {zoom: 1.5, x: 50, y: 27}
            }
        ]
    };