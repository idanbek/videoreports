import { WIDGET_ICONS_URL} from '../urls'

export const DEFAULT_FACEBOOK_FIELDS = [
    {label: 'Impressions', value: 'impressions'},
    {label: 'Clicks', value: 'clicks'},
    {label: 'Cost Per Unique Click', value: 'cost_per_unique_click'},
];

export default {
    name: 'Single Metric',
    widgetType: 'SIMPLE',
    voiceOver:{
        languageCode:'en-GB',
        voice:'Amy',
        src: 'https://s3.amazonaws.com/static.video-reports.text-to-speech/52e09666-009b-4d65-a1ab-da2ae8ca90f4.mp3',
        length: '1.49',
        template: 'This is the single metric.',
        text: 'This is the single metric.'
    },
    version: 0,
    size: {height: 1, width: 1},
    position: {x: 0, y: 0},
    decorators: [{
            type: 'div',
            className: 'decSingleMetric',
            style: {}
        }],
    labels: [
        {
            text: DEFAULT_FACEBOOK_FIELDS[0].label,
            themeStyle:{color:2},
            style: {
                height: '40px',
                fontSize: '48px',
                lineHeight: '40px',
                textAlign: 'left',
                transform: 'matrix(1, 0, 0, 1, 90, 100)',
                zIndex: 3,
                fontFamily: 'Montserrat',
                width: '360px',
            }
        },
        {
            text: '0',
            style: {
                height: '55px',
                fontSize: '66px',
                lineHeight: '67px',
                fontFamily: 'Kalam',
                fontWeight: 'bold',
                textAlign: 'left',
                transform: 'matrix(1, 0, 0, 1, 90, 150)',
                zIndex: 3,
                width: '360px',
            }
        },
        // end data labels
    ],
    images: [
        {
            style: {
                transform: 'matrix(1, 0, 0, 1, 22, 150)',
                height: '50px',
                zIndex: 3,
                width: '50px',
            },
            iconID: 'graph',
            src: `${WIDGET_ICONS_URL}graph.svg`,
            coordinates: {zoom: 1, x: 0, y: 0}
        }
    ]
};