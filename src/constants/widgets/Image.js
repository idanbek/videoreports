export default {
    name: 'Image',
    widgetType: 'SIMPLE',
    voiceOver: {
        languageCode: 'en-GB',
        voice: 'Amy',
        src: 'https://s3.amazonaws.com/static.video-reports.text-to-speech/52e09666-009b-4d65-a1ab-da2ae8ca90f4.mp3',
        length: '1.49',
        template: 'This is the single metric.',
        text: 'This is the single metric.'
    },
    version: 0,
    size: {height: 2, width: 2},
    position: {x: 0, y: 0},
    decorators: [],
    labels: [],
    images: [
        {
            style: {
                transform:'matrix(1,0,0,1,20,20)',
                height: '500px',
                zIndex: 3,
                width: '920px',
            },
            src: 'http://i2.cdn.cnn.com/cnnnext/dam/assets/170822235920-08-trump-phoenix-0822-exlarge-169.jpg',
            coordinates: {zoom: 1.55, x: 50, y: 50}
        }
    ]
};