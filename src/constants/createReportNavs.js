import moment from 'moment'
import { BASE_URL } from './urls'

const BACKGROUND_URL = BASE_URL + '/static/images/backgrounds/';

export const BACKGROUNDS = [
     { backgroundImage: BACKGROUND_URL + 'dots.svg' , backgroundGradient:'linear-gradient(to bottom, rgba(96, 87, 132, .4) ,rgba(200, 241, 234, .4))'} ,
    { backgroundImage: BACKGROUND_URL + 'dots.svg' , backgroundGradient:'linear-gradient(to bottom, rgba(226, 135, 243, .4),rgba(149, 76, 254, .4))'} ,
    { backgroundImage: BACKGROUND_URL + 'lines0.svg' } ,
    { backgroundImage: BACKGROUND_URL + 'lines1.svg' } ,
    { backgroundImage: BACKGROUND_URL + 'lines2.svg' } ,
    { backgroundImage: BACKGROUND_URL + 'lines3.svg' } ,
    { backgroundImage: BACKGROUND_URL + 'lines4.svg' } ,
    { backgroundImage: BACKGROUND_URL + 'simple-grid.svg' } ,
    { backgroundImage: BACKGROUND_URL + 'squares0.svg' } ,
    { backgroundImage: BACKGROUND_URL + 'squares1.svg' } ,
    { backgroundImage: BACKGROUND_URL + 'squares2.svg' } ,
    { backgroundImage: BACKGROUND_URL + 'squares3.svg' } ,
    { backgroundImage: BACKGROUND_URL + 'squares4.svg' } ,
];



export const DATE_FORMAT = 'YYYY-MM-DD';

export const TIME_RANGES = [
    [
        'Last 7 Days' ,
        [
            moment().subtract(6 , 'days').format(DATE_FORMAT) ,
            moment().format(DATE_FORMAT)
        ]
    ] ,
    [
        'Last 30 Days' ,
        [
            moment().subtract(29 , 'days').format(DATE_FORMAT) ,
            moment().format(DATE_FORMAT)
        ]
    ] ,
    [
        'Last Month' ,
        [
            moment().subtract(1 , 'month').startOf('month').format(DATE_FORMAT) ,
            moment().subtract(1 , 'month').endOf('month').format(DATE_FORMAT)
        ]
    ]
];

export const FONT_FAMILIES = [
     { value: 'Kalam' , label: 'Kalam' , style: { fontFamily: 'Kalam, cursive' } } ,
    { value: 'Permanent Marker' , label: 'Permanent Marker' , style: { fontFamily: 'Permanent Marker' } } ,
    { value: 'Lato, sans-serif' , label: 'Lato' , style: { fontFamily: 'Lato, sans-serif' } } ,
    { value: 'Oswald, sans-serif' , label: 'Oswald' , style: { fontFamily: 'Oswald, sans-serif' } } ,
    { value: 'Lobster, cursive' , label: 'Lobster' , style: { fontFamily: 'Lobster, cursive' } } ,
    { value: 'Merriweather, serif' , label: 'Merriweather' , style: { fontFamily: 'Merriweather, serif' } } ,
    { value: 'Abril Fatface, cursive' , label: 'Abril Fatface' , style: { fontFamily: 'Abril Fatface, cursive' } } ,
    { value: 'Alfa Slab One, cursive' , label: 'Alfa Slab One' , style: { fontFamily: 'Alfa Slab One, cursive' } } ,
    { value: 'Arvo, serif' , label: 'Arvo, serif' , style: { fontFamily: 'Arvo, serif' } } ,
    { value: 'Fira Sans, sans-serif' , label: 'Fira Sans' , style: { fontFamily: 'Fira Sans, sans-serif' } } ,
    {
        value: 'Playfair Display, serif' ,
        label: 'Playfair Display' ,
        style: { fontFamily: 'Playfair Display, serif' }
    } ,
    { value: 'Raleway, sans-serif' , label: 'Raleway' , style: { fontFamily: 'Raleway, sans-serif' } } ,
    { value: 'Roboto, sans-serif' , label: 'Roboto' , style: { fontFamily: 'Roboto, sans-serif' } } ,
    { value: 'Montserrat, sans-serif' , label: 'Montserrat' , style: { fontFamily: 'Montserrat, sans-serif' } } ,
];