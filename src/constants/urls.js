// Base.
export const BASE_URL = '//video-reports.topvid.com';
export const VIDEO_API_URL = BASE_URL + '/video-api';

// Integrations.
// Facebook integrations
export const GET_AD_PREVIEW = BASE_URL + '/integrations/facebook/preview/';
export const CREATE_FACEBOOK_URL = BASE_URL + '/integrations/facebook/get-token';
// Slack integrations
export const GET_ALL_SLACK_CHANNELS = BASE_URL + '/integrations/slack/get-channels';
export const POST_SLACK_MESSAGE = BASE_URL + '/integrations/slack/post-message';
export const UPLOAD_SLACK_FILE = BASE_URL + '/integrations/slack/upload-file';
export const POST_EMAIL_MESSAGE = BASE_URL + '/integrations/email/send-mail';

export const GET_INTEGRATIONS_URL = BASE_URL + '/integrations/';

export const GET_ALL_INTEGRATIONS = BASE_URL + '/integrations/get-integrations';

// Video Api shit.
export const GET_VIDEOS_URL = VIDEO_API_URL + '/get-videos';
export const GET_PREVIEW_URL = VIDEO_API_URL + '/video-preview';
export const POST_DRAFT_URL = VIDEO_API_URL + '/save-draft';
export const GET_DRAFTS_URL = VIDEO_API_URL + '/get-drafts';
export const SET_VIDEO_AVAILABILITY = VIDEO_API_URL + '/set-link-availability/'
export const GET_VIDEO_HTML_LINK_URL = VIDEO_API_URL + '/generate-link/';
export const MODIFY_VIDEO_URL = VIDEO_API_URL + '/modify-video';
export const MODIFY_VIDEO_UPLOADED_URLS = VIDEO_API_URL + '/modify-uploaded-urls';
export const DELETE_VIDEO_URL = VIDEO_API_URL + '/delete-video';
export const GET_SPEECH_URL = VIDEO_API_URL + '/get-speech';
export const GENERATE_MP4_URL = VIDEO_API_URL + '/generate-mp4';
export const GET_VIDEO_STATUS_URL = VIDEO_API_URL + '/get-video-data/';

// Images endpoint.
export const WIDGET_ICONS_URL = BASE_URL + '/static/images/widgetIcons/';
export const DEFAULT_IMAGES_URL = BASE_URL + '/static/images/default-images/';

// Others.
export const LOGOUT_URL = BASE_URL + '/logout/';
export const CLOUDINARY_UPLOAD_URL = 'https://api.cloudinary.com/v1_1/dzorggfln/image/upload';