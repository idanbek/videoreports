export const NAV_ITEMS = [
    {
        text: 'Create report',
        link: '/',
    },
    {
        text: 'My Reports',
        link: '/my-reports',
    },
    {
        text: 'My Drafts',
        link: '/my-drafts',
    },
];