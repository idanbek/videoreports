import { find, cloneDeep } from 'lodash'
import generateID from '../utils/IDGenerator'
// widgets
import Image from './widgets/Image'
import Chart from './widgets/Chart'
import Title from './widgets/Title'
import AdBox from './widgets/AdBox'
import Team from './widgets/Team'
import MainTitle from './widgets/MainTitle'
import SingleMetric from './widgets/SingleMetric'
import DoubleMetric from './widgets/DoubleMetric'
import TripleMetric from './widgets/TripleMetric'
import MetricNotebook from './widgets/MetricNotebook'
// Dummy photos
import AdButton from '../images/icons/WidgetButtons/ad_box.png'
import TitleButton from '../images/icons/WidgetButtons/title.png'
import ChartButton from '../images/icons/WidgetButtons/chart.png'
import SmallImage from '../images/icons/WidgetButtons/small_image.png'
import SingleMetricButton from '../images/icons/WidgetButtons/single_metric.png'
import DoubleMetricButton from '../images/icons/WidgetButtons/double_metric.png'
import TripleMetricButton from '../images/icons/WidgetButtons/triple_metric.png'

export const NO_DATA_WIDGETS = [ 'Main title' , 'Title' , 'image' ];

export const WIDGETS_CONFIG = {
    'Single Metric': {image: SingleMetricButton, fieldsNum: 1},
    'Double Metric': {image: DoubleMetricButton, fieldsNum: 2},
    'Triple Metric': {image: TripleMetricButton, fieldsNum: 3},
    'Metric Notebook': {image: DoubleMetricButton, fieldsNum: 3},
    'Ad Box': {image: AdButton, fieldsNum: 3},
    'Title': {image: TitleButton},
    'Chart': {image: ChartButton, fieldsNum: 1},
    'Main title': {image: TitleButton},
    'Image': {image: SmallImage},
    // 'Team': {image: SmallImage},
};

export const ALL_WIDGETS = [
    // Team,
    SingleMetric,
    DoubleMetric,
    TripleMetric,
    MetricNotebook,
    AdBox,
    Title,
    Chart,
    MainTitle,
    Image,
];

export const DEFAULT_FACEBOOK_FIELDS = [
    {label: 'Impressions', value: 'impressions'},
    {label: 'Clicks', value: 'clicks'},
    {label: 'Cost Per Unique Click', value: 'cost_per_unique_click'},
];

export function getWidget(name, version = 0) {
    let widget = find(ALL_WIDGETS, {name, version});
    if (widget) {
        widget['id'] = generateID();
        return cloneDeep(widget);
    }
    return null;
}