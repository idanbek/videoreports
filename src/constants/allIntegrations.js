import GoogleLogo from '../images/img/logos/download.png'
import SheetsLogo from '../images/img/logos/sheets_logo.png'
import FacebookLogo from '../images/img/logos/Facebook.png'
import IntercomLogo from '../images/img/logos/Intercom_logo.png'


export default [
    {
        name: 'Upload',
        id: 'user-upload',
        upload: true,
        redirect: '',
        imageSrc: SheetsLogo,
    },
    {
        name: 'Analytics',
        id: 'google-analytics',
        accountsEndpoint: 'integrations/ga/get-accounts',
        dataEndpoint : 'integrations/ga/get-data',
        dataTypes: ['Session','Bounce Rate', 'Device'],
        redirect: 'https://accounts.google.com/o/oauth2/v2/auth?scope=https%3A%2F%2Fwww.googleapis.com%2Fauth%2Fanalytics.readonly&access_type=offline&redirect_uri=http%3A%2F%2F127.0.0.1%3A8000%2Foauth2%2Foauth2callback&response_type=code&client_id=567530691526-0h0a5hbeddco3d8lijtvf9b7qnvg5g73.apps.googleusercontent.com',
        imageSrc: GoogleLogo,
    },
    {
        name: 'Intercom',
        id: 'intercom',
        redirect: '/integrations/intercom/get-token',
        imageSrc: IntercomLogo,
    },
    {
        name: 'Facebook',
        id: 'facebook',
        redirect: '/integrations/facebook/get-token',
        imageSrc: FacebookLogo,
    }
]