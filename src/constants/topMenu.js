import ResetButtonImg from '../images/icons/globalSettingButtons/delete_btn.png'
import RefreshButtonImg from '../images/icons/globalSettingButtons/refresh_btn.png'
import BackgroundButtonImg from '../images/icons/globalSettingButtons/color_btn.png'
import CalendarButtonImg from '../images/icons/globalSettingButtons/calendar_btn.png'

export const DROP_DOWN_ITEMS = [
    {name: 'Color', img: BackgroundButtonImg, focusItem: {type: 'bg', path: 'bg'}},
    {name: 'Date', img: CalendarButtonImg, focusItem: {type: 'date', path: 'dates'}},
    {name: 'Refresh data', img: RefreshButtonImg, focusItem: 'refresh'},
    {name: 'Reset report', img: ResetButtonImg, focusItem: 'reset'},
];