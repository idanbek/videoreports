import React from 'react'
import axios from 'axios'
import Spinner from 'react-spinkit'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { withRouter } from 'react-router-dom'
// Local imports
import { GET_VIDEOS_URL } from '../../../constants/urls'
import { loadVideo } from '../../../state/activeVideo/actions'
import UserVideo from '../../../shared/UserVideo.react'


class MyReports extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            isLoading: true,
            videos: []
        }
    }

    componentDidMount() {
        axios.get(GET_VIDEOS_URL)
            .then((response) => {
                this.setState({
                    isLoading: false,
                    videos: response.data
                })
            }).catch(() => {
                this.setState({isLoading: false});
            });
    }

    handleClick(nextPath, videoJson) {
        let { history:{ push }, loadVideo} = this.props;
        loadVideo(videoJson);
        push(nextPath);
    }


    render() {
        let { videos, isLoading } = this.state;

        return (
            <div id="DraftsWrapper">
                <div id="draftsTitle">My Reports</div>
                { isLoading ?
                    <Spinner name="chasing-dots" color="blue"/>
                    :
                    videos.reverse().map(({video_json:video, id, date}, idx) =>
                        <UserVideo videoID={ id }
                                   draft={ video }
                                   date={ date }
                                   key={ idx }
                                   onPublish={ this.handleClick.bind(this, '/publish', video) }
                                   onCustomize={ this.handleClick.bind(this, '/', video) }/>
                    )
                }
            </div>
        );
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        loadVideo
    }, dispatch);
}

export default withRouter(connect(null, mapDispatchToProps)(MyReports));