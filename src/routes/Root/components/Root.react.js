import React from 'react'
import { Route, Switch, withRouter } from 'react-router-dom'

import TopBar from './TopBar.react'
import SideBar from './SideBar.react'
import DraftView from '../../Drafts'
import ExportVideo from '../../Publish'
import MyReports from '../../MyReports'
import CreateReport from '../../CreateReport'


export default withRouter((props) => {
    let { history:{ location:{ pathname } } } = props;

    return (
        <div className={ pathname === '/' && 'miniLeftMenu' }>
            <SideBar pathname={ pathname }/>
            <TopBar/>
            <div className="mainContainer">
                <Switch>
                    <Route exact path="/" component={ CreateReport }/>
                    <Route path="/publish" component={ ExportVideo }/>
                    <Route path="/my-drafts" component={ DraftView }/>
                    <Route path="/my-reports" component={ MyReports }/>
                </Switch>
            </div>
        </div>
    );
});