import React from 'react'
import axios from 'axios'
import { OverlayTrigger, Tooltip } from 'react-bootstrap'

import { NAV_ITEMS } from '../../../constants/navItems'
import { LOGOUT_URL, BASE_URL } from '../../../constants/urls'
import TopVidIcon from '../../../images/icons/topvid-icon.png'
import TopVidLogo from '../../../images/icons/topvid-logo.png'


export default function SideBar(props) {
    let { pathname } = props;

    return (
        <div id="sidebar-menu" className="sideBarMenuContainer">
            <nav className="left-menu">
                <div className="left-menu-head">
                    <div className="margin-top-20">
                        <a href="#/" className="logo">
                            <img src={ TopVidIcon } alt=""/>
                        </a>
                    </div>

                    <img className="TopVidLogo" src={ TopVidLogo } alt=""/>

                    <h3>Video Reports</h3>

                    <div className="line-separator-menu"/>
                </div>

                <div className="left-menu-inner scroll-pane">
                    <ul className="left-menu-list left-menu-list-root list-unstyled">
                        { NAV_ITEMS.map((navItem, idx) =>
                            pathname === '/' ?
                                <OverlayTrigger placement="right"
                                                key={ idx }
                                                overlay={ <Tooltip id={ idx }>{ navItem.text }</Tooltip> }>
                                    <li key={ idx } className="left-menu-list-active">
                                        <a className={ 'left-menu-link ' + (pathname === navItem.link && ' active-left-menu') }
                                           href={ `#${navItem.link}` }>
                                            <i className="left-menu-link-icon fa fa-arrow-circle-o-right"/>
                                        </a>
                                    </li>
                                </OverlayTrigger>
                                :
                                <li key={ idx } className="left-menu-list-active">
                                    <a className={ 'left-menu-link ' + (pathname === navItem.link && ' active-left-menu') }
                                       href={ `#${navItem.link}` }>
                                        <i className="left-menu-link-icon fa fa-arrow-circle-o-right"/>
                                        <span>{ navItem.text }</span>
                                    </a>
                                </li>
                        )}

                        { pathname === '/' ?
                            <OverlayTrigger placement="right"
                                            overlay={ <Tooltip id="logoutTooltip">Logout</Tooltip> }>
                                <li className="left-menu-list-active">
                                    <a className="left-menu-link"
                                       onClick={ () => {axios.get(LOGOUT_URL).then(() => {window.location.href = BASE_URL});} }>
                                        <i className="left-menu-link-icon fa fa-power-off"/>
                                    </a>
                                </li>
                            </OverlayTrigger>
                            :
                            <li className="left-menu-list-active">
                                <a className="left-menu-link"
                                   onClick={ () => {axios.get(LOGOUT_URL).then(() => {window.location.href = BASE_URL});} }>
                                    <i className="left-menu-link-icon fa fa-power-off"/>
                                    <span>Logout</span>
                                </a>
                            </li>
                        }
                    </ul>
                </div>
            </nav>
        </div>
    );
}