import React from 'react'
import { Button, Modal } from 'react-bootstrap'


class AddRecipients extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            showModal: false
        }
    }

    toggleModal() {
        this.setState({showModal: !this.state.showModal});
    }

    render() {
        let { showModal } = this.state;

        return (
            <div>
                <Button className="autoBtn" onClick={ this.toggleModal.bind(this) }>
                    <i className="fa fa-refresh pull-left" aria-hidden="true"/>
                    Automate this report
                </Button>

                <Modal id="AutomateModal" show={ showModal } onHide={ this.toggleModal.bind(this) }>
                    <Modal.Header closeButton>
                        <Modal.Title>Add recipients</Modal.Title>
                    </Modal.Header>

                    <Modal.Body>
                        Hello World
                    </Modal.Body>
                </Modal>
            </div>
        );
    }
}

export default AddRecipients;