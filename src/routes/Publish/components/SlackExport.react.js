import React , { Component } from 'react'
import axios from 'axios'
import { Button } from 'react-bootstrap'

import Publish from './Publish.react'
import {
    GET_ALL_INTEGRATIONS ,
    UPLOAD_SLACK_FILE ,
} from "../../../constants/urls";
import SlackChannelSelector from '../../../shared/SlackChannelSelector.react'
import ConnectToSlack from "./ConnectToSlack.react"


export default class SlackExport extends Component {

    constructor(props) {
        super(props);
        this.state = {
            slackConnected: 'PENDING' ,
        };

        this.sendSlackMessage = this.sendSlackMessage.bind(this);
        this.setStateAsync = this.setStateAsync.bind(this);
    }

    setStateAsync(state) {
        return new Promise((resolve) => {
            this.setState(state , resolve)
        });
    }


    async componentDidMount() {
        let { data: usersCurrentIntegrations } = await axios.get(GET_ALL_INTEGRATIONS);

        this.setState({ slackConnected: Object.keys(usersCurrentIntegrations).includes('slack') })
    }


    async sendSlackMessage() {
        let { mp4Links: { mp4_url: video_url } } = this.props;
        let { slackChannelSelected } = this.state;

        // Valid input case
        if (slackChannelSelected) {
            await this.setStateAsync({ uploadingToSlack: true });

            let channels = slackChannelSelected.map(user => user.id).reduce((a , b) => `${a},${b}`);
            try {
                await axios.post(UPLOAD_SLACK_FILE , { video_url , channels });
                Publish.showToast('Video sent' , 'success')
            } catch (e) {
                Publish.showToast('Server error, please try again later' , 'error')
            } finally {
                this.setState({ uploadingToSlack: false });
            }
        } else {
            Publish.showToast('Please select some users' , 'error')
        }
    }


    render() {
        let { slackConnected , uploadingToSlack } = this.state ,
            { show } = this.props;

        return !show ? null : <div>
            {/* Slack is not connected */}
            {(slackConnected === 'PENDING' || !slackConnected) ? <ConnectToSlack slackConnected={slackConnected}/> :
                <div id='SlackExportWrapper'>
                    <div className="title">Share your report in slack.</div>

                    {/*// Slack is connected! w00t w00t.*/}
                    <SlackChannelSelector onChange={(slackChannelSelected) => {
                        this.setState({ slackChannelSelected })
                    }}/>

                    <Button disabled={uploadingToSlack}
                            onClick={this.sendSlackMessage}>
                        { uploadingToSlack ? 'Processing...' : 'Send' }
                    </Button>
                </div>
            }
        </div>;
    }
}



