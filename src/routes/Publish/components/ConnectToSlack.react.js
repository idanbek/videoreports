import React, {Component} from 'react'
import Spinner from 'react-spinkit'

export default class ConnectToSlack extends Component {

    constructor(props){
        super(props);
        this.state = {

        }
    }

    toggleShowPicker(){
        this.setState((prevState)=>({...prevState,showPicker:!prevState.showPicker }))
    }


    render() {
        let { slackConnected } = this.props;

        return slackConnected === 'PENDING' ?
            <Spinner name="chasing-dots" color="steelblue"/> :
            <a href="https://slack.com/oauth/authorize?scope=users:read,channels:read,files:write:user,chat:write:bot&client_id=3066840677.260248933986">
                <img alt="Add to Slack" height="40" width="139"
                     src="https://platform.slack-edge.com/img/add_to_slack.png"
                     srcset="https://platform.slack-edge.com/img/add_to_slack.png 1x, https://platform.slack-edge.com/img/add_to_slack@2x.png 2x"/>
            </a>

    }
}
