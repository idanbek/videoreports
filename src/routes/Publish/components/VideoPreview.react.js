import React, {Component} from 'react'
import axios from 'axios'
import {GET_PREVIEW_URL} from "../../../constants/urls";
import ActiveVideoUtils from "../../../state/activeVideo/utils";
import {connect} from "react-redux";
import {bindActionCreators} from "redux";


class VideoPreview extends Component {

    async componentDidMount() {

        let { zoom,activeVideo,widgetDatas } = this.props;
        const video_json = ActiveVideoUtils.injectDataInGraphs(activeVideo,widgetDatas);

        // Fetch preview.
        let { data:iframeContent } = await axios.post(GET_PREVIEW_URL, {video_json, zoom});

        this.iframe.contentWindow.document.write(iframeContent);
    }

    render() {
        return (
        <div id='PublishPreviewWrapper'>
            <div className='title'>Watch Preview</div>
            <iframe title="Publish Preview" width="100%" height="100%" frameBorder="0"
                           id="publishPreviewVideo"
                           ref={(element) => {
                               this.iframe = element
                           }}
            />
        </div>
        )
    }
}

function mapStateToProps(state) {
    return {

        activeVideo: state.activeVideo,
        widgetDatas: state.widgetDatas,
    };
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({

    }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(VideoPreview);

