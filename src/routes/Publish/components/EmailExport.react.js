import React , { Component } from 'react'
import { STORAGE_KEY } from "../../../constants/localStorage";
import ProgressButton , { STATE as BUTTON_STATE } from 'react-progress-button'
import axios from 'axios'


import { GET_VIDEO_HTML_LINK_URL , CLOUDINARY_UPLOAD_URL , POST_EMAIL_MESSAGE } from "../../../constants/urls";
import OnHoverColorPicker from './OnHoverColorPicker.react'

export default class EmailExport extends Component {
    static DEFAULT_SUBJECT = 'Your weekly video report';
    static DEFAULT_TITLE = 'Your weekly video report is ready';
    static DEFAULT_SUBTITLE = 'Let me know if you have any questions';


    static DURATION_ERROR = 1200;
    static DURATION_SUCCESS = 1200;

    constructor(props) {
        super(props);

        // Get theme colors and agent image to be used in image template.
        let { bg: { themeColors } , frames } = window.localStorage.getObject(STORAGE_KEY);

        this.state = {
            status: BUTTON_STATE.LOADING ,
            isUploading: false ,
            logoImage: frames.find(widget => widget.name === 'Main title').images[ 1 ].src ,
            footerColors: [ themeColors[ 0 ] , themeColors[ 1 ] ]
        }
    }

    componentDidMount() {
        let { videoID: id } = this.props;

        // Getting link from the server ASAP.
        axios.get(`${GET_VIDEO_HTML_LINK_URL}${id}`).then((response) => {
            this.setState({
                htmlLink: response.data ,
                status: BUTTON_STATE.NOTHING
            })
        });
    }

    uploadFile(event) {
        let reader = new FileReader();

        reader.onload = (loadEvent) => {
            let src = loadEvent.target.result , formData = new FormData();

            this.setState({ isUploading: true } , () => {
                formData.append('upload_preset' , 'ya8xvjy8');
                formData.append('api_key' , '392132768994591');
                formData.append('api_secret' , 'DTmFTL90aoz2qQWIgVba25gz2Lw');
                formData.append('file' , src);
                formData.append('tags' , window.user_json ? window.user_json.username : 'sergey@kilometer.io');
                axios.post(CLOUDINARY_UPLOAD_URL , formData , {
                    headers: { 'Content-Type': undefined , 'X-Requested-With': 'XMLHttpRequest' }
                }).then((response) => {
                    this.setState({ isUploading: false , logoImage: response.data.secure_url })
                });
            });
        };

        reader.readAsDataURL(event.target.files[ 0 ]);
    }

    handleFormSubmit(e) {

        // Check if form is valid.
        const VALID_FORM = [ this.toMail , this.fromMail , this.message , this.greeting , this.subject ]
            .map(el => el.validity.valid)
            .reduce((a , b) => a && b);

        // Valid input
        if (VALID_FORM) {
            let { footerColors: [ topColor , botColor ] , htmlLink , logoImage } = this.state ,
                { mp4Links:  { scaled_thumbnail_url: posterImage }  } = this.props;

            let formData = {
                'subject': this.subject.value ,
                'from_mail': this.fromMail.value ,
                'to_mail': this.toMail.value ,
                'greeting': this.greeting.value ,
                'message': this.message.value ,
                'top_color': topColor ,
                'bottom_color': botColor ,
                'link_to': htmlLink ,
                'poster_img': posterImage ,
                'logo_img': logoImage
            };

            // Set status to pending.
            this.setState({
                    status: BUTTON_STATE.LOADING
                } ,
                // Then make a request to send mail
                () => {
                    axios.post(POST_EMAIL_MESSAGE , formData)
                        .then((response) => {
                            // If successful change button to successful state for like 1 second.
                            this.setState((prevState) => {
                                    this.toMail.value = '';
                                    return { ...prevState , status: BUTTON_STATE.SUCCESS }
                                } , () => {
                                    setTimeout(() => {
                                        this.setState({ status: BUTTON_STATE.NOTHING })
                                    } , EmailExport.DURATION_SUCCESS)
                                }
                            )
                        })
                });
        }
        // Bad emails case.
        else {
            // Set to bad email
            this.setState((prevState) => {
                return { ...prevState , status: BUTTON_STATE.ERROR }
            } , () => {
                setTimeout(() => {
                    this.setState({ status: BUTTON_STATE.NOTHING })
                } , EmailExport.DURATION_ERROR)
            })

        }
    }

    render() {
        let { show , mp4Links } = this.props ,
            { footerColors , logoImage , status , htmlLink } = this.state;


        return !show ? null :
            <form id='ExportEmailWrapper' action="javascript:myFunction(); return false;">

                <div id='emailTemplate'>
                    <div className='top-bar'>
                        <div/>
                        <div/>
                        <div/>
                    </div>

                    <div className='bot-bar'>
                        <input
                            defaultValue={EmailExport.DEFAULT_SUBJECT}
                            placeholder={'Subject'}
                            required
                            pattern='.+'
                            ref={(input) => {
                                this.subject = input;
                            }}/>
                        <input type='email' required defaultValue={ window.user_json.email } placeholder={'your@email.com'} ref={(input) => {
                            this.fromMail = input;
                        }}/>
                        <div>Reply to email:</div>
                    </div>

                    <div className='top-footer' style={{ backgroundColor: footerColors[ 0 ] }}>
                        <OnHoverColorPicker id='topFooterPicker'
                                            onChange={(color) => {
                                                this.setState({ footerColors: [ color.hex , footerColors[ 1 ] ] })
                                            }}
                                            color={footerColors[ 0 ]}/>
                        <div
                            type='file'
                            className='logo'
                            style={{
                                backgroundImage: `url(${ logoImage })` ,
                                backgroundSize: 'contain'
                            }}/>
                        <input type='file' accept="image/*" onChange={this.uploadFile.bind(this)}/>
                    </div>

                    <div className='body'>
                        <input
                            defaultValue={EmailExport.DEFAULT_TITLE}
                            required
                            placeholder={'Enter title'} pattern='.+'
                            ref={(input) => {
                                this.greeting = input;
                            }}/>
                        <input
                            defaultValue={EmailExport.DEFAULT_SUBTITLE}
                            required
                            style={{}}
                            placeholder={' Enter subtitle '}
                            pattern='.+'
                            ref={(input) => {
                                this.message = input;
                            }}/>

                        <div
                            className='video-preview'
                            onClick={() => {
                                window.open(htmlLink)
                            }}
                            style={{
                                backgroundImage: `url(${mp4Links[ 'scaled_thumbnail_url' ] })` ,

                            }}/>
                    </div>

                    <div className='bottom-footer footer' style={{ backgroundColor: footerColors[ 1 ] }}>

                        <OnHoverColorPicker id='bottomFooterPicker'
                                            onChange={(color) => {
                                                this.setState({ footerColors: [ footerColors[ 0 ] , color.hex ] })
                                            }
                                            } color={footerColors[ 1 ]}/>
                    </div>

                </div>

                <div id='sendEmailSection'>
                    <div>Recipients email:</div>
                    <input required type={'email'} ref={(input) => {
                        this.toMail = input;
                    }}/>
                    <ProgressButton
                        type={'submit'}
                        state={status}
                        durationError={EmailExport.DURATION_ERROR}
                        durationSuccess={EmailExport.DURATION_SUCCESS}
                        onClick={this.handleFormSubmit.bind(this)}>
                        <span className='button-message'>Go</span>
                    </ProgressButton>
                </div>
            </form>
    }
}



