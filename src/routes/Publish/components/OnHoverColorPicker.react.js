import React, {Component} from 'react'
import {CirclePicker} from 'react-color'

export default class EmailExport extends Component {

    constructor(props){
        super(props);
        this.state = {
            showPicker:false
        }
    }

    toggleShowPicker(){
        this.setState((prevState)=>({...prevState,showPicker:!prevState.showPicker }))
    }


    render() {
        let { color, id , onChange } = this.props,
            { showPicker } = this.state;


        return (
            <div className='on-hoover-picker' id={id} onClick={ this.toggleShowPicker.bind(this)}>
                <i className="fa fa-paint-brush" aria-hidden="true"/>
                { showPicker && <CirclePicker color={ color } onChangeComplete={ (color)=>{ onChange(color) }}/> }

            </div>
        )
    }

}
