import React, {Component} from 'react'
import axios from 'axios'
import {Button, ProgressBar} from 'react-bootstrap'
import { DefaultPlayer as Video } from 'react-html5video';

import VideoPreview from './VideoPreview.react';

import {
    GENERATE_MP4_URL,
    GET_VIDEO_STATUS_URL
} from "../../../constants/urls";

export default class Mp4Export extends Component {

    static VIDEO_SCALE = .45;

    constructor(props) {
        super(props);
        this.state = {
            generateVideoPercent: props.isGenerating && 50,
        };

    }

    componentDidMount() {
        let { isGenerating } = this.props;

        if(isGenerating){
            this.checkVideoStatus();
        }

    }

    generateVideo() {
        let { videoID:id } = this.props;

        this.setState(() => ({generateVideoPercent: .1}), () => {
            axios.post(GENERATE_MP4_URL, {id}).then((response) => {
                    this.checkVideoStatus()
                });
        });
    }

    checkVideoStatus() {
        let { videoID , onCompletedVideo } = this.props;

        axios.get(`${GET_VIDEO_STATUS_URL}${videoID}`).then(response => {
            let {data: {status, link: mp4Links}} = response;
            if (['pending', 'in_progress__batch_pending'].includes(status)) {
                this.setState((prevState) => ({
                    ...prevState,
                    generateVideoPercent: Math.min(100, Math.floor(prevState.generateVideoPercent + 3))
                }), () => {
                    setTimeout(() => {
                        this.checkVideoStatus()
                    }, 5000)
                })
            } else if (status === 'done') {
                onCompletedVideo(mp4Links);
            } else {
                alert('ERROR! status:' + status)
            }

        })
    }

    render() {

        let { generateVideoPercent } = this.state,
            { show,  mp4Links } = this.props,
            ExportMp4 = null;

        // Video was allraedy generated.
        if (mp4Links) {
            ExportMp4 = <div id='downloadMp4Wrapper'>
                <div className='title' >Video Ready!</div>
                <a className='btn' href={mp4Links['mp4_url']}>Download MP4</a></div>
        // Video is generating
        } else if (generateVideoPercent) {
            ExportMp4 = <div id='publishProgressBar'>
                <div className='title'>Generating the report video</div>
                <ProgressBar now={ generateVideoPercent }/>
                <div className='progressTitle'>{ `${generateVideoPercent}%` }</div>
            </div>

            // Video not generating
        } else {
            ExportMp4 =
                <Button className='apply-btn' style={{display:'block'}} onClick={this.generateVideo.bind(this)}>Generate MP4</Button>
        }


        return !show? null: <div id='exportMp4'>
            {mp4Links &&
            <Video autoPlay
                   style={{
                       height: `${1080 * Mp4Export.VIDEO_SCALE}px` ,
                       width: `${1920 * Mp4Export.VIDEO_SCALE}px`
                   }}>
                <source src={mp4Links[ 'mp4_url' ]}/>
            </Video>
            }

            { (!generateVideoPercent && !mp4Links ) &&   <VideoPreview zoom={ Mp4Export.VIDEO_SCALE }/> }

            {ExportMp4}
        </div>;
    }
}

