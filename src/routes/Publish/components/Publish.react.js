import React from 'react'
import axios from 'axios'
import { Button } from 'react-bootstrap'
import Spinner from 'react-spinkit'
import { withRouter } from 'react-router-dom'
import { isEqual } from 'lodash'

import { POST_DRAFT_URL , GET_VIDEO_STATUS_URL , MODIFY_VIDEO_URL } from '../../../constants/urls'
import Mp4Export from './Mp4Export.react'
import HtmlExport from './HtmlExport.react'
import SlackExport from './SlackExport.react'
import EmailExport from './EmailExport.react'
import AutomateReport from './AutomateReport.react'
import ActiveVideoUtils from '../../../state/activeVideo/utils'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { sleep } from "../../../utils/utilFunctions";
import { ToastContainer , toast } from "react-toastify";

class Publish extends React.Component {

    static PENDING_STATUSES = [ 'pending' , 'in_progress__batch_pending' ];

    static EXPORT_TYPES = [
        { type: 'mp4' , title: 'Download MP4' } ,
        { type: 'html' , title: 'View Online' } ,
        { type: 'mail' , title: 'Send by email' , needsLinks: true } ,
        { type: 'slack' , title: 'Send to Slack' , needsLinks: true }
    ];

    constructor(props) {
        super(props);
        this.state = {
            activeMenuType: 'mp4' ,
            mp4Links: null ,
            videoID: null ,
            isGenerating: false ,
        };

        this.goBack = this.goBack.bind(this);
        this.onGeneratedVideo = this.onGeneratedVideo.bind(this);
        this.changeMenu = this.changeMenu.bind(this);
    }

    async componentDidMount() {
        // Posts the draft so the components could work with the video in the DB rather than local storage.
        let { widgetDatas , activeVideo } = this.props;
        let { data: { link: mp4Links , status , video_json } } = await axios.get(`${GET_VIDEO_STATUS_URL}${activeVideo[ 'video_id' ]}`);

        if (Publish.PENDING_STATUSES.includes(status)) {
            this.setState({ isGenerating: true , ready: true })
            // If not pending:
        } else {
            // Parse our current json
            let user_json = ActiveVideoUtils.injectDataInGraphs(activeVideo , widgetDatas);

            // If our json is DEEP EQUAL to server JSON change mp4Links if they exist.
            if (isEqual(user_json , video_json)) {
                this.setState({ mp4Links , ready: true })
            } else {
                // If our json is not equal, post our json into the server (AS AN UPDATE OF THE ID)
                await axios.post(MODIFY_VIDEO_URL , { video_json: user_json , id: user_json[ 'video_id' ] });
                this.setState({ ready: true })
            }
        }
    }

    static showToast(toastMessage , type) {
        type ? toast[ type ](toastMessage) : toast(toastMessage);
    }

    goBack() {
        let { history: { push } } = this.props;
        push('/')
    }

    changeMenu(activeMenuType) {
        this.setState({ activeMenuType })
    }

    onGeneratedVideo(mp4Links) {
        this.setState({ mp4Links , isGenerating: false })
    }

    render() {
        let { activeVideo: { video_id: videoID } } = this.props ,
            { activeMenuType , mp4Links , ready , isGenerating } = this.state;

        if (!ready) {
            return <Spinner name="chasing-dots" color="steelblue"/>
        }

        const menuItems = Publish.EXPORT_TYPES.map((exportType , idx) => {
                const DISABLED = !mp4Links && exportType.needsLinks;
                return <div key={idx}
                            onClick={() => {
                                DISABLED ?
                                    Publish.showToast('You must export MP4 first!' , 'error') :
                                    this.changeMenu(exportType.type)
                            }}
                            className={DISABLED ? 'disabled' : exportType.type === activeMenuType ? 'active' : ''}>
                    {exportType.title}
                </div>
            }
        );

        // We have a video ID to work with, its all good!
        return (
            <div id='publishWrapper'>

                <div id='publishTitle'>
                    <Button onClick={ this.goBack }>
                        <i className="fa fa-caret-left"/>
                        Back
                    </Button>
                    <h2 className='title'>Publish Report</h2>
                    <h5 className='subtitle'>This report was saved and can be found on the "My Reports" page</h5>
                </div>

                <div id='publishExportMenu'>
                    {menuItems}
                    <AutomateReport/>
                </div>
                <div className="publishSubmenu">
                    <Mp4Export show={activeMenuType === 'mp4'} isGenerating={isGenerating} videoID={videoID}
                               mp4Links={mp4Links}
                               onCompletedVideo={this.onGeneratedVideo}/>
                    <HtmlExport show={activeMenuType === 'html'} videoID={videoID} mp4Links={mp4Links}/>
                    <EmailExport show={activeMenuType === 'mail'} videoID={videoID} mp4Links={mp4Links}/>
                    <SlackExport show={activeMenuType === 'slack'} videoID={videoID} mp4Links={mp4Links}/>
                </div>

                <ToastContainer
                    position="bottom-center"
                    type="error"
                    autoClose={5000}
                    hideProgressBar
                    newestOnTop={false}
                    closeOnClick
                    pauseOnHover
                />
            </div>
        );
    }
}

function

mapStateToProps(state) {
    return {
        activeVideo: state.activeVideo ,
        widgetDatas: state.widgetDatas ,
    };
}

function

mapDispatchToProps(dispatch) {
    return bindActionCreators({} , dispatch);
}


export default withRouter(connect(mapStateToProps,mapDispatchToProps)(Publish));
