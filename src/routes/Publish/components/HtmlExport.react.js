import React, {Component} from 'react'
import axios from 'axios'
import { Button } from 'react-bootstrap'

import Publish from './Publish.react';
import {
    GET_VIDEO_HTML_LINK_URL,
    SET_VIDEO_AVAILABILITY
} from "../../../constants/urls";

export default class HtmlExport extends Component {

    constructor(props) {
        super(props);

        // Default link message.
        this.state = {
            isSettingPassword:false,
            isSettingAvailability:false,
            htmlLink: 'Generating link. . .',
            showCreateUniqueLink: true,
            showSetPassword: false,
        };

         this.copyLink =  this.copyLink.bind(this);
         this.openLink = this.openLink.bind(this);
         this.addPassword = this.addPassword.bind(this);
         this.toggleUniqueLink = this.toggleUniqueLink.bind(this);
         this.toggleSetPassword = this.toggleSetPassword.bind(this);
         this.setStateAsync = this.setStateAsync.bind(this);
    }

    async componentDidMount() {
        let {videoID: id} = this.props;

        // Getting link from the server ASAP.
        let {data:{link:htmlLink,is_available:showCreateUniqueLink}} = await axios.post(`${GET_VIDEO_HTML_LINK_URL}${id}`,{});
        console.log('SHOW LINK',showCreateUniqueLink)

        this.setState({ htmlLink,showCreateUniqueLink })
    }

      setStateAsync(state) {
        return new Promise((resolve) => {
            this.setState(state , resolve)
        });
    }


    copyLink() {
        let textAreaElement = document.getElementById('dummyTextArea');

        textAreaElement.select();
        document.execCommand('copy');

        Publish.showToast('Link copied to clipboard','info')

    }

    openLink(){
        let { htmlLink } = this.state;
        window.open(htmlLink)
    }

    async toggleUniqueLink(){
        let {showCreateUniqueLink:prevShowCreateUniqueLink} = this.state,
            {videoID} = this.props;

        await this.setStateAsync({showCreateUniqueLink:!prevShowCreateUniqueLink,isSettingAvailability:true});

        try {
            await axios.post(`${SET_VIDEO_AVAILABILITY}${videoID}` , { is_available: !prevShowCreateUniqueLink });
            Publish.showToast(`Link is now ${!prevShowCreateUniqueLink? 'Available':'Unavailable'}`,'success')
        }catch(e){
            Publish.showToast('Error server, please try again later','error')
        }finally{
            this.setState({isSettingAvailability:false});
        }
    }

    toggleSetPassword(){
        let {showSetPassword:prevShowSetPassword} = this.state;
        this.setState({showSetPassword:!prevShowSetPassword});

    }

    async addPassword(){
        let { videoID: id } = this.props;

        await this.setStateAsync({isSettingPassword:true});

        try {
            await axios.post(`${GET_VIDEO_HTML_LINK_URL}${id}` , { password: this.passwordInput.value });
            Publish.showToast('Password changed','success')
        }catch(e){
            Publish.showToast('Error server, please try again later','error')
        }finally{
            this.setState({isSettingPassword:false});
        }
    }

    render() {
        let { htmlLink,  showCreateUniqueLink, showSetPassword, isSettingPassword, isSettingAvailability  } = this.state,
            { show } = this.props;

        return !show ? null :
            <div id='exportHtmlWrapper'>
                <div className='checkboxContainer'>
                    <input type='checkbox'
                           disabled={ isSettingAvailability }
                           defaultChecked={ showCreateUniqueLink }
                           onChange={ this.toggleUniqueLink } />
                    <span>Make report available via unique link</span>
                </div>

                <hr/>

                { showCreateUniqueLink &&
                <div id='linkContainer'>
                    <div className='instructions'> Copy the following link and share it:</div>
                    <input id='linkBox' value={ htmlLink }/>
                    <textarea id='dummyTextArea' readOnly={true} value={ htmlLink }/>

                    <Button onClick={ this.copyLink }>Copy</Button>
                    <Button onClick={ this.openLink }>Open</Button>

                    <div className='checkboxContainer'>
                        <input type='checkbox'
                               defaultChecked={ showSetPassword }
                               onChange={ this.toggleSetPassword }/>
                        <span>Add Password protection</span>
                    </div>

                    { showSetPassword &&
                        <div id='passwordProtection'>
                            <input type='text' ref={(element)=>{ this.passwordInput = element } }/>
                            <Button disabled={ isSettingPassword } onClick={this.addPassword}>{isSettingPassword? 'Processing...': 'Set' }</Button>
                        </div>
                    }
                </div>
                }
            </div>


    }
}

