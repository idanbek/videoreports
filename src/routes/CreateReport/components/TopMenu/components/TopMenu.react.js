import React from 'react'
import axios from 'axios'
import { map,isEmpty } from 'lodash'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { withRouter } from 'react-router-dom'
import { Button , Dropdown , MenuItem , ButtonToolbar } from 'react-bootstrap'

import Preview from './Preview.react'
import { GET_VIDEO_STATUS_URL , POST_DRAFT_URL } from '../../../../../constants/urls'
import { STORAGE_KEY , WIDGET_DATAS } from '../../../../../constants/localStorage'
import { DROP_DOWN_ITEMS } from '../../../../../constants/topMenu'
import { setFocusItem } from '../../../../../state/focusItem/actions'
import { getWidget , WIDGETS_CONFIG } from '../../../../../constants/allWidgets'
import { addNewWidget , setIn } from '../../../../../state/activeVideo/actions'
import { fetchWidgetDatas } from '../../../../../state/widgetDatas/actions'
import SaveOptionsInput from '../../../../../shared/SaveOptionsInput.react'
import SaveButtonImg from '../../../../../images/icons/globalSettingButtons/save_btn.png'


class TopMenu extends React.PureComponent {

    constructor(props) {
        super(props);

        this.state = {
            allowSaveDraft:true,
        };

        this.handleScroll = this.handleScroll.bind(this);
        this.handleSaveDraft = this.handleSaveDraft.bind(this);
        this.handleTopBarClick = this.handleTopBarClick.bind(this);
        this.handleGoToPublish = this.handleGoToPublish.bind(this);
        this.addNewWidget = this.addNewWidget.bind(this);

    }


    async componentDidMount(){
        let { videoJson } = this.props;

        // Case json is not in DB then he was not saved
        if(videoJson[ 'video_id' ]){
            let { data: { link } } = await axios.get(`${GET_VIDEO_STATUS_URL}${videoJson[ 'video_id' ]}`);
            this.setState({allowSaveDraft:isEmpty(link)})
        }

    }

    addNewWidget(name) {
        let { addNewWidget: addNewWidgetAction , widgetDatas } = this.props;
        let defaultData;

        try {
            let { campaign_id , account_id , level , item_id } = widgetDatas[ Object.keys(widgetDatas)[ 0 ] ];
            defaultData = {campaign_id , account_id , level , item_id};
        // No past data.
        }catch(e){
            defaultData = {}
        }finally{
            addNewWidgetAction(name,defaultData)
        }


    }

    async handleSaveDraft(name) {
        let { videoJson , history: { push } } = this.props;
        videoJson[ 'name' ] = name;

        await axios.post(POST_DRAFT_URL , { video_json: videoJson , template_version: 22 });

        push('/my-drafts');
    }

    handleTopBarClick(focusItem) {
        let { setFocusItem , fetchWidgetDatas , setIn , videoJson: { frames } } = this.props;

        if (focusItem === 'refresh') {
            fetchWidgetDatas();
        } else if (focusItem === 'reset') {
            setIn([ 'frames' ] , [ frames[ 0 ] ])
        } else {
            setFocusItem(focusItem);
        }
    }

    handleScroll(side) {
        if (side === 'right') {
            this.refs.widgetBtns.scrollLeft += 110;
        } else {
            this.refs.widgetBtns.scrollLeft -= 110;
        }
    }

    handleGoToPublish(name) {
        let { history: { push } , videoJson ,setIn, widgetDatas } = this.props;
        setIn('name',name);

        localStorage.setObject(STORAGE_KEY , videoJson);
        localStorage.setObject(WIDGET_DATAS , widgetDatas);

        push('/publish');
    }

    render() {
        let { videoJson , videoJson: { name } , widgetDatas } = this.props,
            { allowSaveDraft } = this.state;

        return (
            <div id="topMenu">
                <Preview videoJson={videoJson} widgetDatas={widgetDatas}/>

                <div className="widgetBtnsWrapper">
                    <button className="scrollBtn"
                            onClick={() => {
                                this.handleScroll('left')
                            }}>
                        <i className="fa fa-angle-left fa-lg" aria-hidden="true"/>
                    </button>

                    <div className="widgetBtns" ref="widgetBtns">
                        {map(WIDGETS_CONFIG , ({ image } , name) =>
                            <div className="addWidgetButtonWrapper" key={name}>
                                <button className="widgetBtn"
                                        onClick={() => {
                                            this.addNewWidget(name)
                                        }}>
                                    <img src={image} alt=""/>
                                </button>

                                <div className="titleContainer">{name}</div>
                            </div>
                        )}
                    </div>

                    <button className="scrollBtn"
                            onClick={() => {
                                this.handleScroll('right')
                            }}>
                        <i className="fa fa-angle-right fa-lg" aria-hidden="true"/>
                    </button>
                </div>

                <ButtonToolbar className="pull-right">

                    {allowSaveDraft && <Button id='SaveButton' className="btnToolBar">
                        <img onClick={this.handleSaveDraft} src={SaveButtonImg} alt=""/>
                        <SaveOptionsInput placeholder="Enter draft's name"
                                          onClicked={this.handleSaveDraft}/>
                    </Button>}

                    <Dropdown id="dropdown-custom-1">
                        <Dropdown.Toggle noCaret className="btnToolBar">
                            <i className="fa fa-cog fa-lg" aria-hidden="true"/>
                        </Dropdown.Toggle>

                        <Dropdown.Menu className="super-colors">
                            {DROP_DOWN_ITEMS.map((val , idx) =>
                                <MenuItem key={idx}
                                          eventKey={idx}
                                          onClick={() => {
                                              this.handleTopBarClick(val.focusItem)
                                          }}>
                                    <img src={val.img} alt=""/>
                                    <span className="margin-left-10">{val.name}</span>
                                </MenuItem>
                            )}
                        </Dropdown.Menu>
                    </Dropdown>

                    <Button id="PublishReportButton" className="btn-next"
                            onClick={() => {
                                name && this.handleGoToPublish(name)
                            }}>
                        Publish
                        <span className="next-arrow margin-left-10"/>
                        {!name && <SaveOptionsInput placeholder="Enter video's name"
                                                    onClicked={this.handleGoToPublish}/>}
                    </Button>
                </ButtonToolbar>
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        widgetDatas: state.widgetDatas ,
        videoJson: state.activeVideo
    };
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        setFocusItem ,
        setIn ,

        addNewWidget ,
        fetchWidgetDatas ,
    } , dispatch);
}

export default withRouter(connect(mapStateToProps , mapDispatchToProps)(TopMenu));