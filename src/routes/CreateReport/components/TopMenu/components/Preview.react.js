import React from 'react'
import axios from 'axios'
import { Modal } from 'react-bootstrap'

import { GET_PREVIEW_URL } from '../../../../../constants/urls'
import PreviewButtonImg from '../../../../../images/icons/play_preview.png'
import ActiveVideoUtils from '../../../../../state/activeVideo/utils'


class Preview extends React.PureComponent {

    constructor(props) {
        super(props);
        this.state = {
            showModal: false
        }
    }

    closePreview() {
        this.setState({showModal: false});
    }

    openPreview() {
        let { videoJson, widgetDatas } = this.props;

        let video_json = ActiveVideoUtils.injectDataInGraphs(videoJson,widgetDatas);

        this.setState({showModal: true},() => {
              let zoom = (document.querySelectorAll('#PreviewModal > .modal-dialog > .modal-content > .modal-body')[0].clientWidth - 3) / 1920;
              axios.post(GET_PREVIEW_URL, {video_json, zoom})
                    .then((response) => {
                        document.getElementById('previewVideo').contentWindow.document.write(response.data);
                    });
        });
    }

    render() {
        let { showModal } = this.state;

        return (
            <div className="previewWrapper pull-left">
                <img src={ PreviewButtonImg } onClick={ this.openPreview.bind(this) } alt=""/>

                <p>Preview</p>

                { showModal &&
                    <Modal id="PreviewModal" show={ showModal } onHide={ this.closePreview.bind(this) }>
                        <Modal.Header closeButton>
                            <Modal.Title>Video preview</Modal.Title>

                            <span className="sub-title">
                                This is preview mode. The final HD video will offer better quality and smoother motion.
                            </span>
                        </Modal.Header>

                        <Modal.Body>
                            <iframe title="Video Preview" width="100%" height="100%" frameBorder="0" id="previewVideo"/>
                        </Modal.Body>
                    </Modal>
                }
            </div>
        );
    }
}

export default Preview;