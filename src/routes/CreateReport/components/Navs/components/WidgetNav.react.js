import React from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { Button } from 'react-bootstrap'
import { isEqual , clone , get , sortBy } from 'lodash'

import NavHeader from './shared/NavHeader.react'
import TitledSelector from './TitledSelector.react'
import allMetrics from '../../../../../constants/allMetrics'
import { WIDGETS_CONFIG } from '../../../../../constants/allWidgets'
import { fetchMetrics } from '../../../../../state/dataSources/actions'
import { editWidgetData } from '../../../../../state/widgetDatas/actions'


const FACEBOOK_LEVELS = {
    'account': { topLevel: 'account' } ,
    'campaign': { topLevel: 'account' } ,
    'adset': { topLevel: 'campaign' } ,
    'ad': { topLevel: 'adset' }
};

class WidgetNav extends React.PureComponent {

    constructor(props) {
        super(props);
        this.state = {
            musicPlaying: false ,
            metrics: allMetrics ,
            facebook: clone(props.widgetData) ,
        };
        this.changeWidgetData = this.changeWidgetData.bind(this);
        this.handleChangeFacebook = this.handleChangeFacebook.bind(this);
    }

    componentDidMount() {
        let { widgetData , fetchMetrics } = this.props;
        if (widgetData && widgetData.account_id) {
            fetchMetrics('facebook' , { account_id: widgetData.account_id })
        }
    }

    componentWillReceiveProps(nextProps) {
        let { widgetData , fetchMetrics } = this.props;

        if (!isEqual(nextProps.widgetData , widgetData)) {
            if (nextProps.widgetData) {
                fetchMetrics('facebook' , { account_id: nextProps.widgetData.account_id });
            }
            this.setState({ facebook: clone(nextProps.widgetData) });
        }
    }

    handleChangeFacebook(option) {
        let { facebook } = this.state ,
            { fetchMetrics } = this.props;
        // set facebook option
        if (option) {
            if (facebook.account_id !== option.obj.account_id) {
                fetchMetrics('facebook' , { account_id: option.obj.account_id })
            }
            this.setState({
                facebook: { ...option.obj , item_id: option.obj.id , fields: facebook.fields , level: option.level }
            });
        } else {
            let newFacebookData = clone(facebook) ,
                topLevel = FACEBOOK_LEVELS[ facebook.level ].topLevel;
            // remove facebook option
            delete newFacebookData[ `${facebook.level}_id` ];
            this.setState({
                facebook: { ...newFacebookData , level: topLevel , item_id: facebook[ `${topLevel}_id` ] }
            });
        }
    }

    changeWidgetFields(idx , field) {
        let { facebook } = this.state ,
            fields = clone(facebook.fields);
        fields[ idx ] = field;
        this.setState({ facebook: { ...facebook , fields: fields } })
    }

    changeWidgetData() {
        let { editWidgetData , widget } = this.props ,
            { facebook } = this.state;
        facebook[ 'widgetId' ] = widget.id;
        editWidgetData(facebook);
    }

    static renderOption(option) {
        return (
            <div>
                <i className={`fa fa-${option.status === 'ACTIVE' ? 'play' : 'pause' }-circle`}
                   aria-hidden="true"/>
                <span className="margin-left-5">{option.label}</span>
            </div>
        );
    }

    render() {
        let { widget , voiceOverMenu , facebookSource } = this.props ,
            { facebook } = this.state;

        return (
            <NavHeader title={widget.name}
                       icon={
                           <img src={get(WIDGETS_CONFIG , [ widget.name , 'image' ])}
                                className="pull-left margin-top-5 margin-right-10" alt=""/>
                       }>
                {/*NEEDS TO STAY HERE*/}
                {voiceOverMenu}
                {/*DO NOT MOVE*/}
                {facebookSource && facebook &&
                <div className="side-bar-content">
                    <div className="subMenuTitle">Source</div>
                    <div className="text-line font-line"/>
                    <div id="DataSourceContainer">

                        <TitledSelector
                            title="Ad Account"
                            placeholder="Filter by AdAccount"
                            clearable={false}
                            options={facebookSource.accounts
                                .map(acc => {
                                    return { value: acc.id , label: acc.name , obj: acc , level: 'account' }
                                })
                            }
                            value={facebook.account_id}
                            onChange={this.handleChangeFacebook}/>

                        <TitledSelector
                            title="Campaign"
                            placeholder="Filter by Campaign"
                            options={facebookSource.campaigns
                                .filter(camp => camp.account_id === facebook.account_id)
                                .map(camp => {
                                    return { value: camp.id , label: camp.name , obj: camp , level: 'campaign' }
                                })
                            }
                            value={facebook.campaign_id}
                            optionRenderer={WidgetNav.renderOption}
                            onChange={this.handleChangeFacebook}/>

                        {facebook.campaign_id &&
                        <div>
                            <TitledSelector
                                title="Adset"
                                placeholder="Filter by Ad Set"
                                options={facebookSource.ad_sets
                                    .filter(adset => adset.campaign_id === facebook.campaign_id)
                                    .map(adset => {
                                        return { value: adset.id , label: adset.name , obj: adset , level: 'adset' }
                                    })}
                                value={facebook.adset_id}
                                optionRenderer={WidgetNav.renderOption}
                                onChange={this.handleChangeFacebook}/>

                            {facebook.adset_id &&
                            <TitledSelector
                                title="Ad"
                                placeholder="Filter by Ad"
                                options={facebookSource.ads
                                    .filter(ad => ad.adset_id === facebook.adset_id)
                                    .map(ad => {
                                        return { value: ad.id , label: ad.name , obj: ad , level: 'ad' }
                                    })}
                                value={facebook.ad_id}
                                optionRenderer={WidgetNav.renderOption}
                                onChange={this.handleChangeFacebook}/>
                            }
                        </div>
                        }
                    </div>

                    <div className="subMenuTitle margin-top-20">Metrics</div>
                    <div className="text-line font-line"/>

                    {facebook.fields.map((field , idx) =>
                        <TitledSelector
                            title={`Metric ${idx + 1}`}
                            clearable={false}
                            key={idx}
                            options={sortBy(allMetrics.concat(facebookSource.metrics || []) , 'label')}
                            value={field.value}
                            onChange={this.changeWidgetFields.bind(this , idx)}/>
                    )}

                    <Button className="apply-btn"
                            onClick={this.changeWidgetData}>
                        Apply
                    </Button>

                </div>
                }


            </NavHeader>
        );
    }
}

function mapStateToProps(state , ownProps) {
    let { dataSources , widgetDatas } = state ,
        widgetData = widgetDatas[ ownProps.widget.id ];

    return {
        facebookSource: dataSources.facebook ,
        widgetData
    };
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        editWidgetData ,
        fetchMetrics
    } , dispatch);
}

export default connect(mapStateToProps , mapDispatchToProps)(WidgetNav);