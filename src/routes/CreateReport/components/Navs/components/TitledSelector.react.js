import React from 'react'
import Select from 'react-select'


export default function TitledSelector(props) {
    let { clearable , options , value , onChange , placeholder , title} = props;

    return (
        <div className="facebook-selector">
            <div className="title">{ title }</div>

            <Select placeholder={ placeholder }
                    className="DataSelector"
                    clearable={ clearable }
                    options={ options }
                    value={ value }
                    onChange={ onChange }/>
        </div>
    );
}
