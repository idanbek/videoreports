import React from 'react'
import axios from 'axios'
import Spinner from 'react-spinkit'
import { FormGroup, ControlLabel, FormControl, Button } from 'react-bootstrap'

import FitItem from './shared/FitItem.react'
import NavHeader from './shared/NavHeader.react'
import RangeInput from '../../../../../shared/RangeInput.react'
import fitSelectedImage from '../../../../../utils/fitSelectedImage'
import { CLOUDINARY_UPLOAD_URL } from '../../../../../constants/urls'


class ImageNav extends React.PureComponent {

    constructor(props){
        super(props);
        this.state = {
            isUploading: false
        };
        this.onChangeZoom = this.onChangeZoom.bind(this);
        this.changeImage = this.changeImage.bind(this);
        this.onImageUpload = this.onImageUpload.bind(this) ;
    }

    componentDidMount() {
        let featherEditor = new window.Aviary.Feather({
            apiKey: '1234567',
            onSave: function (imageID, newURL) {
                let { image } = this.props;
                image.src = newURL;
                this.changeImage(image);
                featherEditor.close();
            }.bind(this)
        });
        document.getElementById('imageEdit').onclick = function () {
            let { image: { src } } = this.props;
            function launchEditor() {
                featherEditor.launch({image: 'uploadImageImg', url: src});
                return false;
            }
            return launchEditor();
        }.bind(this);
    }

    changeImage(args) {
        let { path, setIn, image } = this.props;
        setIn(path, { ...image, ...args });
    }

    onChangeZoom(newZoom) {
        let { image } = this.props;
        image.coordinates.zoom = newZoom;
        this.changeImage(image);
    }

    onImageUpload(event) {
        let reader = new FileReader();

        reader.onload = (loadEvent) => {
            let src = loadEvent.target.result, formData = new FormData();

            this.setState({isUploading: true});
            formData.append('upload_preset', 'ya8xvjy8');
            formData.append('api_key', '392132768994591');
            formData.append('api_secret', 'DTmFTL90aoz2qQWIgVba25gz2Lw');
            formData.append('file', src);
            formData.append('tags', window.user_json ? window.user_json.username : 'sergey@kilometer.io');

            axios.post(CLOUDINARY_UPLOAD_URL, formData, {
                headers: {'Content-Type': undefined, 'X-Requested-With': 'XMLHttpRequest'}
            }).then((response) => {
                let newImg = new Image();
                newImg.onload = () => {
                    this.setState({isUploading: false});
                    document.getElementById('customImageUploader').value = '';
                    this.changeImage({src: newImg.src});
                    this.changeImage({coordinates: fitSelectedImage(newImg)});
                };
                newImg.src = response.data.secure_url;
            });
        };

        reader.readAsDataURL(event.target.files[0]);
    }

    render() {
        let { image, voiceOverMenu } = this.props,
            { isUploading } = this.state;

        return (
            <NavHeader title="Image"
                       icon={ <i className="fa fa-picture-o fa-lg margin-top-5 pull-left"/> }>
               { voiceOverMenu }
                <div className='side-bar-content'>
                    <div className="image-upload">
                        { isUploading ?
                            <Spinner name="three-bounce" color="blue"/>
                            :
                            <img id="uploadImageImg" src={ image.src } alt=""/>
                        }
                    </div>

                    <div className="btn-repl-wrapper">
                        <FormGroup controlId="customImageUploader" className="margin-bottom-0">
                            <ControlLabel className="btn btn-repl-img">Replace Picture</ControlLabel>

                            <FormControl onChange={ this.onImageUpload }
                                         className="hidden"
                                         accept="image/*"
                                         type="file"/>
                        </FormGroup>

                        <Button className="btn-edit-img margin-top-20 margin-bottom-20" id="imageEdit">
                            <i className="fa fa-pencil" aria-hidden="true"/>
                            Edit Picture
                        </Button>
                    </div>

                    <div className="rangeInputs">
                        <RangeInput max={ 3 }
                                    value={ image.coordinates.zoom }
                                    label="Zoom"
                                    handleChange={ this.onChangeZoom }/>

                        <FitItem item={ image } handleChange={ this.changeImage }/>
                    </div>
                </div>

            </NavHeader>
        );
    }
}

export default ImageNav;