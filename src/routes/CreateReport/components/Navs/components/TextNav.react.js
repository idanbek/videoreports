import React from 'react'
import {Button} from 'react-bootstrap'


import ColorBar from './shared/ColorBar.react'
import TextAlign from './shared/TextAlign.react'
import NavHeader from './shared/NavHeader.react'
import TextOptions from './shared/TextOptions.react'



class TextNav extends React.PureComponent {

    constructor(props){
        super(props);


        this.changeLabelStyle = this.changeLabelStyle.bind(this);
        this.handleLabelColorChange = this.handleLabelColorChange.bind(this);
        this.handleFontSizeChange = this.handleFontSizeChange.bind(this);
        this.changeFocusToData = this.changeFocusToData.bind(this);
    }

    changeLabelStyle(newStyle) {
        let { setIn, path, label } = this.props;
        label.style = {...label.style, ...newStyle } ;
        setIn(path, {...label});
    }

    handleLabelColorChange(newColor) {
        let { setIn, path, label } = this.props;
        label['style']['color'] = newColor;
        setIn(path, {...label});
    }

    handleFontSizeChange(fontSize) {
        let { setIn, path, label } = this.props;
        label['style']['fontSize'] = fontSize;
        label['isCustomFontSize'] = true;
        setIn(path, {...label});
    }

    changeFocusToData(){
        let { path , setFocusItem } = this.props;

        setFocusItem({type: 'widget', path: path.slice(0,2)})

    }

    render() {
        let { label:{ style }, voiceOverMenu , widgetData } = this.props;

        return (
            <NavHeader title="Text"
                       icon={ <i className="fa fa-font fa-lg margin-top-5 pull-left"/> }>
                 { voiceOverMenu }
                <div className="side-bar-content">
                    <div className="subMenuTitle margin-bottom-10">Font</div>

                    <TextOptions style={ style }
                                 handleFontSizeChange={ this.handleFontSizeChange }
                                 handleChange={ this.changeLabelStyle }/>

                    <TextAlign textAlign={ style.textAlign }
                               handleChange={ this.changeLabelStyle }/>

                    <ColorBar selectedColor={ style.color }
                              handleChange={ this.handleLabelColorChange }/>

                    { widgetData && <Button className='change-data-btn' onClick={this.changeFocusToData}>Edit Data Source</Button>}


                </div>

            </NavHeader>
        );
    }
}

export default TextNav;