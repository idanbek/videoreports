import React from 'react'
import get from 'lodash/get'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import BGNav from './BGNav'
import DateNav from './DateNav.react'
import IconNav from './IconNav.react'
import TextNav from './TextNav.react'
import ImageNav from './ImageNav.react'
import WidgetNav from './WidgetNav.react'
import VoiceOverMenu from './VoiceOverMenu.react'
import { setFocusItem } from '../../../../../state/focusItem/actions'
import { setVoiceEditor, updateVoiceOver, playVoiceOver } from '../../../../../state/voiceEditor/actions'
import { setIn, setDates } from '../../../../../state/activeVideo/actions'


class Navs extends React.PureComponent{

    render() {
        let { updateVoiceOver, playVoiceOver, voiceEditor, focusItem:{ type, path }, item, setIn, frames,
                setVoiceEditor, setFocusItem, setDates, widgetDatas } = this.props,
            Nav = null;

        switch (type) {
            case 'widget':
                Nav = <WidgetNav path={ path }
                                 setIn={ setIn }
                                 widget={ item }/>;
                break;
            case 'text':
                Nav = <TextNav setIn={ setIn }
                               label={ item }
                               path={ path }
                               setFocusItem={ setFocusItem }
                               widgetData={ widgetDatas[frames[path[1]].id] }/>;
                break;
            case 'image':
                Nav = <ImageNav setIn={ setIn }
                                image={ item }
                                path={ path }/>;
                break;
            case 'icon':
                Nav = <IconNav setIn={ setIn }
                               image={ item }
                               path={ path }/>;
                break;
            case 'bg':
                Nav = <BGNav bg={ item }
                             setIn={ setIn }/>;
                break;
            case 'date':
                Nav = <DateNav dates={ item }
                               setDates={ setDates }/>;
                break;
            default:
                alert('No navush :(')
        }

        return (
            <div className="nav-section-wrapper no-padding box-shadow">
                <div id="navigationSectionId" className="inline">
                       { path instanceof Array && path[0] === 'frames' && type !== 'icon' &&
                        <VoiceOverMenu voiceEditor={ voiceEditor }
                                       widgetId={ frames[path[1]].id }
                                       voiceOver={ frames[path[1]].voiceOver }
                                       setVoiceEditor={ setVoiceEditor }
                                       updateVoiceOver={ updateVoiceOver }
                                       playVoiceOver={ playVoiceOver }/>
                    }

                    { Nav }

                </div>
            </div>
        );
    }
}

function mapStateToProps(state) {
    let { focusItem , activeVideo , widgetDatas, voiceEditor } = state;

    // Make sure that a widget is actually chosen.
    return {
        voiceEditor: voiceEditor,
        frames: activeVideo.frames,
        focusItem: focusItem,
        widgetDatas,
        item: get(activeVideo, focusItem.path)
    };
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        setIn,
        setFocusItem,
        setDates,
        setVoiceEditor,
        updateVoiceOver,
        playVoiceOver
    } , dispatch);
}

export default connect(mapStateToProps , mapDispatchToProps)(Navs);