import React from 'react'
import { isEqual } from 'lodash'
import { SketchPicker } from 'react-color'

import NavHeader from './shared/NavHeader.react'
import BackIcon from '../../../../../images/navs/icon-background.png'
import { initSchemaColors } from '../../../../../utils/initFunctions'
import { BACKGROUNDS } from '../../../../../constants/createReportNavs'

class BGNav extends React.PureComponent {

    constructor(props) {
        super(props);
        let { bg: { themeColors } } = props;
        this.state = {
            schemaColors: initSchemaColors(themeColors)
        };

        this.handleBackgroundChange = this.handleBackgroundChange.bind(this);
        this.onToggleColorPicker = this.onToggleColorPicker.bind(this);
        this.onChangeColor = this.onChangeColor.bind(this);
        this.handleClose = this.handleClose.bind(this);
    }


    handleBackgroundChange(args) {
        let { setIn , bg } = this.props;

        setIn([ 'bg' ] , {...bg,...args});
    }

    onToggleColorPicker(colorIdx , color) {
        let { showColorPicker } = this.state;

        this.setState({
            sketchColor: color ,
            sketchColorIdx: colorIdx ,
            showColorPicker: !showColorPicker
        })
    }

    handleClose() {
        this.setState({ showColorPicker: false })
    };

    onChangeColor(newColor) {
        let { themeColors } = this.props ,
            { sketchColorIdx } = this.state;

        themeColors[ sketchColorIdx ] = newColor;
        this.setState({ sketchColor: newColor });
        this.setIn([ 'bg' , 'themeColors' ] , themeColors);
    }

    static patternClass(pattern , backgroundImage) {
        let temp = backgroundImage.replace('url(' , '').replace(')' , '');
        return 'backGroundChoice' + (pattern === temp ? ' activeFilmPicChoice outline-5' : '')
    }

    render() {
        let { bg: { themeColors , backgroundImage:pickedBackgroundImage, backgroundGradient:pickedBackgroundGradient } } = this.props ,
            { schemaColors , showColorPicker , sketchColor } = this.state;

        return (
            <NavHeader title="Background"
                       icon={<img className="pull-left" src={BackIcon} alt=""/>}>
                <div className="side-bar-content">
                    <div className="pull-left">Color scheme</div>

                    <hr className="margin-top-15 pull-left" width="91%"/>

                    <div className="row">
                        {schemaColors.map((schemaColor , indx) => {
                            let colors = schemaColor.slice(0 , themeColors.length);
                            return (
                                <div key={indx}
                                     className={'color-bar-bg pull-left' + (isEqual(colors , themeColors) ? ' theme-selected' : '')}
                                     onClick={()=>{ this.handleBackgroundChange( { themeColors: colors }) }}>
                                    {colors.map((color , idx) =>
                                        <div key={idx} className="colorBox" style={{ backgroundColor: color }}/>
                                    )}
                                </div>
                            );
                        })}

                        <div id="patterns-schema" className="pull-left">
                            {themeColors.map((color , colorIdx) =>
                                <div key={colorIdx} className="colorPickerSection pull-left">
                                    <button className="Plus"
                                            type="button"
                                            onClick={()=>{ this.onToggleColorPicker(colorIdx,color)}}
                                            style={{ backgroundColor: color }}>
                                        +
                                    </button>
                                </div>
                            )}

                            {showColorPicker &&
                            <div className="blurWrapper margin-top-40">
                                <div className="blurDiv" onClick={()=>{this.handleClose()}}/>

                                <SketchPicker color={sketchColor}
                                              onChange={(color) => {
                                                  this.onChangeColor(color.hex)
                                              }}/>
                            </div>
                            }
                        </div>
                    </div>

                    <hr className="margin-top-15 pull-left" width="91%"/>

                    <h4>Patterns</h4>

                    {BACKGROUNDS.map(({ backgroundImage , backgroundGradient = null } , idx) =>
                        <div key={idx}
                             className={'backGroundChoice' + (backgroundImage === pickedBackgroundImage &&
                             (!pickedBackgroundGradient || pickedBackgroundGradient === backgroundGradient) ? ' activeFilmPicChoice outline-5' : '')}
                             onClick={()=>{ this.handleBackgroundChange( {backgroundImage , backgroundGradient }) }}>
                            <div className="backGroundChoiceImg"
                                 style={{ backgroundSize: 'cover' ,  backgroundImage: `url(${backgroundImage})${backgroundGradient? `,${backgroundGradient}`:''}` }}/>
                        </div>
                    )}
                </div>
            </NavHeader>
        );
    }
}

export default BGNav;