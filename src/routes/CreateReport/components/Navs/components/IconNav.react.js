import React from 'react'

import { ICONS } from '../constants/Icons'
import NavHeader from './shared/NavHeader.react'


class IconNav extends React.PureComponent {

    constructor(props){
        super(props);

        this.changeImage = this.changeImage.bind(this);
    }

    changeImage(args) {
        let { path, setIn, image } = this.props;
        setIn(path, { ...image, ...args });
    }

    render() {
        let { image } = this.props;

        return (
            <NavHeader title="Icons"
                       icon={ <i className="fa fa-meh-o fa-lg margin-top-5 pull-left"/> }>
                <div className="side-bar-content">
                    <div id="icons-wrapper">
                        { ICONS.map(([id, url], idx) =>
                            <div key={ idx }
                                 className={ 'icon-wrapper' + (image.iconID === id ? ' icon-selected' : '') }>
                                <div className="icon-option"
                                     onClick={ ()=>{this.changeImage({src:url, iconID:id})} }
                                     style={{ backgroundImage: `url(${url})` }}/>
                            </div>
                        )}
                    </div>
                </div>
            </NavHeader>
        );
    }
}

export default IconNav;