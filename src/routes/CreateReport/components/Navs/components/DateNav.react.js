import React from 'react'
import moment from 'moment'
import DateRangePicker from 'react-bootstrap-daterangepicker'

import NavHeader from './shared/NavHeader.react'
import { DATE_FORMAT, TIME_RANGES } from '../../../../../constants/createReportNavs'


class DateNav extends React.PureComponent {

    changeDate(startDate, endDate){
        let { setDates } = this.props;

        setDates([startDate, endDate]);
    }

    handleRangeChange(event, {startDate, endDate}) {
        this.changeDate(
            startDate.format(DATE_FORMAT),
            endDate.format(DATE_FORMAT)
        )
    }

    render() {
        let { dates } = this.props,
            RangePicked = TIME_RANGES.find(([,[fromDate,toDate]])=> fromDate === dates[0] && toDate === dates[1]) || ['Custom Range',[dates[0],dates[1]]],
            IS_CUSTOM = RangePicked[0] === 'Custom Range';

        return (
            <NavHeader title="Date"
                       icon={
                           <i className="fa fa-calendar pull-left margin-top-5 margin-right-10" aria-hidden="true"/>
                       }>
                <div className="side-bar-content">
                    <div className="subMenuTitle">Date Picker</div>
                    <hr className="margin-top-15 pull-left" width="91%"/>

                    <div id='timeRangeWrapper'>
                        { TIME_RANGES.map(([timeName,[startDate, endDate]],idx)=>
                            <div key={ idx }
                                 className={ `timeRangeOption ${timeName === RangePicked[0] ? 'active' : ''}` }
                                 onClick={ this.changeDate.bind(this, startDate, endDate) }>
                                { timeName }
                            </div>
                        )}

                        <DateRangePicker startDate={ moment(dates[0], DATE_FORMAT) }
                                         endDate={ moment(dates[1], DATE_FORMAT) }
                                         onApply={ this.handleRangeChange.bind(this) }>
                            <div className={ `timeRangeOption ${IS_CUSTOM ? 'active' : ''}` }>
                                Custom Range
                            </div>
                        </DateRangePicker>

                        <div className={ `customDateContainer ${IS_CUSTOM ? 'active' : ''}` }>
                            <div>
                                From
                                <div>{ IS_CUSTOM && dates[0] }</div>
                            </div>

                            <div>
                                To
                                <div>{ IS_CUSTOM && dates[1] }</div>
                            </div>
                        </div>
                    </div>
                </div>
            </NavHeader>
        );
    }
}

export default DateNav;