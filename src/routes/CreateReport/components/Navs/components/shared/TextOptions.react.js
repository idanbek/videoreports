import React from 'react'
import Select from 'react-select'
import { ButtonToolbar, Button } from 'react-bootstrap'

import { initFontSizes } from '../../../../../../utils/initFunctions'
import { FONT_FAMILIES } from '../../../../../../constants/createReportNavs'

class TextOptions extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            fontSizes: initFontSizes(5, 100)
        }
    }

    onChangeBtn(key, value) {
        let { style, handleChange } = this.props;
        if (style.hasOwnProperty(key)) {
            delete style[key]
        } else {
            style[key] = value;
        }
        handleChange(style);
    }

    onChange(key, option) {
        let { style, handleChange } = this.props;
        style[key] = option.value;
        handleChange(style);
    }

    handleFontSizeChange(option) {
        let { handleFontSizeChange } = this.props;
        handleFontSizeChange(option.value);
    }

    render() {
        let { style: { fontFamily, fontSize, fontWeight, fontStyle, textDecoration } } = this.props,
            { fontSizes } = this.state;

        return (
            <div>
                <Select name="form-field-name"
                        className="margin-bottom-20"
                        clearable={ false }
                        value={ FONT_FAMILIES.find(fontOptionObj=>fontOptionObj.label.startsWith(fontFamily)) }
                        options={ FONT_FAMILIES }
                        onChange={ this.onChange.bind(this, 'fontFamily') }/>

                <Select name="form-field-name"
                        clearable={ false }
                        value={ fontSize }
                        options={ fontSizes }
                        onChange={ this.handleFontSizeChange.bind(this) }/>

                <ButtonToolbar className="margin-top-20">
                    <Button className={ (fontWeight === 'bold' ? 'active': '') }
                            onClick={this.onChangeBtn.bind(this, 'fontWeight', 'bold')}>
                        <i className="fa fa-bold" aria-hidden="true"/>
                    </Button>

                    <Button className={ (fontStyle === 'italic' ? 'active': '') }
                            onClick={ this.onChangeBtn.bind(this, 'fontStyle', 'italic') }>
                        <i className="fa fa-italic" aria-hidden="true"/>
                    </Button>

                    <Button className={ (textDecoration === 'line-through' ? 'active': '') }
                            onClick={ this.onChangeBtn.bind(this, 'textDecoration', 'line-through') }>
                        ABC
                    </Button>

                    <Button className={ (textDecoration === 'underline' ? ' active': '') }
                            onClick={ this.onChangeBtn.bind(this, 'textDecoration', 'underline') }>
                        <i className="fa fa-underline" aria-hidden="true"/>
                    </Button>
                </ButtonToolbar>
            </div>
        );
    }
}

export default TextOptions;