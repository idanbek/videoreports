import React from 'react'

import LeftAlign from '../../../../../../images/navs/right-align-button.png'
import RightAlign from '../../../../../../images/navs/left-align-button.png'
import CenterAlign from '../../../../../../images/navs/center-align-button.png'


class TextAlign extends React.Component {

    handleChange(event) {
        let { handleChange } = this.props;
        handleChange({textAlign : event.target.value});
    }

    render() {
        let { textAlign='center', className='' } = this.props;

        return (
            <div className={ 'margin-top-10 ' + className }>
                <div className="inline radioButton">
                    <input type="radio"
                           id="leftAlignRadioButton"
                           onChange={ this.handleChange.bind(this) }
                           checked={ textAlign === 'left' }
                           value="left"/>

                    <label htmlFor="leftAlignRadioButton">
                        <img className="textAlignImage" src={ LeftAlign } alt=""/>
                    </label>
                </div>

                <div className="inline radioButton">
                    <input type="radio"
                           id="centerAlignRadioButton"
                           onChange={ this.handleChange.bind(this) }
                           checked={ textAlign === 'center' }
                           value="center"/>

                    <label htmlFor="centerAlignRadioButton">
                        <img className="textAlignImage" src={ CenterAlign } alt=""/>
                    </label>
                </div>

                <div className="inline radioButton">
                    <input type="radio"
                           id="rightAlignRadioButton"
                           onChange={ this.handleChange.bind(this) }
                           checked={ textAlign === 'right' }
                           value="right"/>

                    <label htmlFor="rightAlignRadioButton">
                        <img className="textAlignImage" src={ RightAlign } alt=""/>
                    </label>
                </div>
            </div>
        );
    }
}

export default TextAlign;