import React from 'react'

export default (props) => {
    let { children, icon, title } = props;

    return (
        <div>
            <div className="side-bar-title margin-bottom-20">
                { icon }
                <span className="pull-left">{ title }</span>
            </div>

            { children }
        </div>
    );
}