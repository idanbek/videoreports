import React from 'react'
import { Button } from 'react-bootstrap'

import FloatingSelector from '../../../../../shared/FloatingSelector'
import { POLLY_MAPPING, LANGUAGES, IS_PLAYING, IS_LOADING, IS_PAUSED } from '../../../../../constants/pollyMapping'


class VoiceOverMenu extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            showVoiceMenu: true
        };
        this.pauseVoiceOver = this.pauseVoiceOver.bind(this);
        this.toggleVoiceMenu = this.toggleVoiceMenu.bind(this);
        this.handleVoiceChange = this.handleVoiceChange.bind(this);
        this.handleLanguageChange = this.handleLanguageChange.bind(this);
        this.handleAnnotationChange = this.handleAnnotationChange.bind(this);
    }

    componentWillReceiveProps(nextProps) {
        let { voiceEditor, widgetId, setVoiceEditor, voiceOver:{ isSaving } } = this.props,
            { widgetId:nextWidgetId, voiceOver:{ isSaving:nextIsSaving, src:nextSrc } } = nextProps;

        if (nextWidgetId === widgetId && isSaving && !nextIsSaving) {
            setVoiceEditor({...voiceEditor, src: nextSrc});
        }
    }

    componentDidUpdate(prevProps) {
        let { voiceEditor:{ status } } = this.props,
            { voiceEditor:{ status:prevStatus } } = prevProps;

        if (prevStatus !== IS_PLAYING && status === IS_PLAYING) {
            document.getElementById('VoiceoverAudio').play();
        }
    }

    handleLanguageChange({voices, value}) {
        let { voiceEditor, setVoiceEditor } = this.props,
            newVoiceEditor = {languageCode: value};
        if (!voices.includes(voiceEditor.voice)) {
            newVoiceEditor['voice'] = voices[0];
        }
        setVoiceEditor({...voiceEditor, ...newVoiceEditor});
    }

    handleVoiceChange({value}) {
        let { voiceEditor, setVoiceEditor } = this.props;
        setVoiceEditor({...voiceEditor, voice: value});
    }

    handleAnnotationChange(event) {
        let { voiceEditor, setVoiceEditor } = this.props;
        setVoiceEditor({...voiceEditor, template: event.target.value});
    }

    pauseVoiceOver() {
        let { voiceEditor, setVoiceEditor } = this.props,
            musicElement = document.getElementById('VoiceoverAudio');
        musicElement.pause();
        musicElement.currentTime = 0;
        setVoiceEditor({...voiceEditor, status: IS_PAUSED});
    }

    toggleVoiceMenu() {
        this.setState({showVoiceMenu: !this.state.showVoiceMenu})
    }

    render() {
        let { voiceEditor:{ languageCode, src, template, voice, status }, updateVoiceOver, playVoiceOver } = this.props,
            { showVoiceMenu } = this.state,
            voiceOptions = POLLY_MAPPING[languageCode].voices.map(v => {return {value: v, label: v}});

        return (
            <div id="VoiceSubmenu" className={ showVoiceMenu ? 'open' : 'closed' }>
                <div id="VoiceHeader">
                    <i className={ `fa fa-angle-${showVoiceMenu ? 'down' : 'up'}` }
                       aria-hidden="true"
                       onClick={ this.toggleVoiceMenu }/>
                    <span>Annotation</span>

                    <i className={`fa fa-microphone ${status === IS_PLAYING ? 'animate' : ''}`}
                       aria-hidden="true"/>

                    <i className="fa fa-commenting-o" aria-hidden="true">
                        <FloatingSelector id="VoiceSelector"
                                          onPicked={ this.handleVoiceChange }
                                          selected={ voice }
                                          options={ voiceOptions }/>
                    </i>

                    <i className="fa fa-globe" aria-hidden="true">
                        <FloatingSelector id="LanguageSelector"
                                          onPicked={ this.handleLanguageChange }
                                          selected={ languageCode }
                                          options={ LANGUAGES }/>
                    </i>
                </div>

                <div id="VoiceContent" className={ showVoiceMenu ? '' : 'hidden' }>
                    <textarea id="VoiceoverInput"
                              spellCheck={ false }
                              value={ template }
                              onChange={ this.handleAnnotationChange }/>

                    <audio id="VoiceoverAudio" src={ src } onEnded={ this.pauseVoiceOver }/>

                    <span id="PlayVoiceover" className={ status === IS_LOADING ? 'loading' : '' }
                          onClick={ status === IS_PLAYING ? this.pauseVoiceOver : playVoiceOver }>
                            <i className={ `fa fa-${status === IS_PLAYING ? 'pause' : 'play'}` }
                               aria-hidden="true"/>
                    </span>

                    <Button className="apply-btn voice-apply-btn"
                            onClick={ updateVoiceOver }>
                        Save
                    </Button>
                </div>
            </div>
        )
    }
}

export default VoiceOverMenu;