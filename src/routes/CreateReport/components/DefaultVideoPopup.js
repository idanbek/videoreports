import React from 'react'
import axios from 'axios'
import { Button } from 'react-bootstrap'
import Spinner from 'react-spinkit'
import Select from 'react-select'

import { NO_DATA_WIDGETS } from "../../../constants/allWidgets";
import { CREATE_FACEBOOK_URL } from '../../../constants/urls'
import initFacebookReport from '../../../utils/initReport/facebook'
import { FB_SDK , FB_CONFIG , FB_PERMISSIONS } from '../../../constants/defaultVideoPopUp'


class DefaultVideoPopup extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            showLoading: false ,
            fbAccount: {} ,
            fbCampaign: {}
        };
        this.handleSubmit = this.handleSubmit.bind(this);
        this.connectFacebook = this.connectFacebook.bind(this);
        this.handleFBAccountChange = this.handleFBAccountChange.bind(this);
        this.handleFBCampaignChange = this.handleFBCampaignChange.bind(this);
    }

    componentWillMount() {
        let { dataSources } = this.props;
        FB_SDK.init(FB_CONFIG);
        this.setState({ showLoading: dataSources.status.isLoading })
    }

    componentWillReceiveProps(nextProps) {
        let { dataSources: { status: { isLoading } } } = nextProps;
        if (this.props.dataSources.status.isLoading !== isLoading) {
            this.setState({ showLoading: isLoading })
        }
    }

    connectFacebook() {
        this.setState({ showLoading: true });
        FB_SDK.login((response) => {
            if (response.hasOwnProperty('authResponse') &&
                response[ 'authResponse' ].hasOwnProperty('accessToken')) {
                let access_token = response[ 'authResponse' ][ 'accessToken' ];
                axios.post(CREATE_FACEBOOK_URL , { access_token: access_token })
                    .then(({ status , data }) => {
                        if (status === 200) {
                            this.props.addDataSource({ ...data.accounts , type: data.type });
                        } else {
                            this.setState({ showLoading: false });
                        }
                    })
                    .catch((response) => {
                        this.setState({ showLoading: false });
                    });
            }
        } , { scope: FB_PERMISSIONS });
    }

    handleFBAccountChange(option) {
        this.setState({ fbAccount: option });
    }

    handleFBCampaignChange(option) {
        this.setState({ fbCampaign: option });
    }

    handleSubmit() {
        let { addBatchWidgets , generateVideoID , fetchWidgetDatas , setAdBoxImage , changeVoiceOver , dataSources: { facebook: { ads } } } = this.props ,
            { fbCampaign: { value: campID } , fbAccount: { value: accID , label: accName } } = this.state ,
            filteredAds = ads.filter(({ campaign_id }) => campaign_id === campID) ,
            { widgets , widgetDatas } = initFacebookReport(accName , accID , campID , filteredAds);

        // This only takes care of the voice in the data widgets.
        addBatchWidgets(widgets);
        fetchWidgetDatas(widgetDatas);

        // Voice over of main title should always be first.
        // Update sound of dataless widgets.
        widgets.filter(({ name }) =>
            NO_DATA_WIDGETS.concat('Chart').includes(name)
        ).forEach(datalessWidget => {
            console.log('DATALESS',datalessWidget.voiceOver, widgets.findIndex(({ id }) => id === datalessWidget.id))
            changeVoiceOver(datalessWidget.voiceOver , [ 'frames' , widgets.findIndex(({ id }) => id === datalessWidget.id) ])
        })

        // const mainTitleIndex = widgets.filter(w => w.name === 'Main title');
        // changeVoiceOver(widgets[ mainTitleIndex ].voiceOver , [ 'frames' , mainTitleIndex ]);

        // Change image of AD widgets.
        widgets
            .filter(({ name }) => name === 'Ad Box')
            .forEach((adWidget , idx) => {
                setAdBoxImage(filteredAds[ idx ][ 'id' ] , [ 'frames' , widgets.findIndex(({ id }) => id === adWidget.id) ])
            });


        // The id of this video json in the server.
        generateVideoID();
    }

    render() {
        let { fbAccount , fbCampaign , showLoading } = this.state ,
            { dataSources: { facebook: facebookSource } } = this.props;

        return (
            <div id="DefaultReportModal">
                <div className="backgroundContainer"/>
                <div className="DefaultReportWrapper">
                    {showLoading ?
                        <Spinner className="loader" name="chasing-dots" color="white"/>
                        :
                        facebookSource ?
                            <div id="createDefaultReportWrapper">
                                <div className="instructions">Please choose a campaign.</div>

                                <div className="instructions">
                                    We will create a basic report that will include basic metrics from your campaigns
                                </div>

                                <Select placeholder="Select Ad Account"
                                        clearable={false}
                                        options={facebookSource.accounts.map(acc => {
                                            return {
                                                value: acc.id , label: acc.name
                                            }
                                        })}
                                        value={fbAccount.value}
                                        onChange={this.handleFBAccountChange}/>

                                {fbAccount.value &&
                                <Select placeholder="Select Campaign"
                                        clearable={false}
                                        options={facebookSource.campaigns
                                            .filter(camp => camp.account_id === fbAccount.value)
                                            .map(camp => {
                                                return { value: camp.id , label: camp.name }
                                            })
                                        }
                                        value={fbCampaign.value}
                                        onChange={this.handleFBCampaignChange}/>
                                }

                                {fbCampaign.value &&
                                <Button onClick={this.handleSubmit}>Go!</Button>
                                }
                            </div>
                            :
                            <div id="connectFacebookWrapper">
                                <div className="instructions">
                                    The app will be using Facebook ad accounts to run the retargeting campaign.
                                </div>
                                <div className="instructions">Please click on the button to provide access.</div>

                                <div className="connectFBbutton" onClick={this.connectFacebook}>
                                    <i className="fa fa-facebook" aria-hidden="true"/>
                                    <span>Link Facebook Account</span>
                                </div>
                            </div>
                    }
                </div>
                }
            </div>
        )
    }
}

export default DefaultVideoPopup;