import React from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import Navs from './Navs'
import TopMenu from './TopMenu'
import Report from './Report'
import DefaultVideoPopup from './DefaultVideoPopup'
import { fetchWidgetDatas } from '../../../state/widgetDatas/actions'
import { setAdBoxImage, addBatchWidgets, generateVideoID, changeVoiceOver } from '../../../state/activeVideo/actions'
import { fetchDataSources, addDataSource } from '../../../state/dataSources/actions'


class CreateReport extends React.Component {

    componentWillMount() {
        let { fetchDataSources, fetchWidgetDatas } = this.props;
        fetchDataSources();
        fetchWidgetDatas();
    }

    render() {
        let { showVideoPopup, dataSources, fetchWidgetDatas, generateVideoID, setAdBoxImage, addDataSource, addBatchWidgets, changeVoiceOver } = this.props;

        return (
            <div>
                <TopMenu/>
                <section id="Builder" className="page-content">
                    { showVideoPopup &&
                        <DefaultVideoPopup dataSources={ dataSources }
                                           generateVideoID={ generateVideoID }
                                           changeVoiceOver={ changeVoiceOver }
                                           setAdBoxImage={ setAdBoxImage }
                                           addDataSource={ addDataSource }
                                           addBatchWidgets={ addBatchWidgets }
                                           fetchWidgetDatas={ fetchWidgetDatas }/>
                    }
                    <Report/>
                    <Navs/>
                </section>
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        dataSources: state.dataSources,
        showVideoPopup: (state.activeVideo.frames.length === 0),
    };
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        fetchDataSources,
        fetchWidgetDatas,
        generateVideoID,
        setAdBoxImage,
        addBatchWidgets,
        addDataSource,
        changeVoiceOver
    }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(CreateReport);

