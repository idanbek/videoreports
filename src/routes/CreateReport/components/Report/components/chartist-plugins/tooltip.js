import Chartist from 'chartist';

function ToolTip (options) {


        let defaultOptions = {
            valueTransform: Chartist.noop,
            seriesName: true // Show name of series in tooltip.
        };

        options ={...defaultOptions,options};

        return function tooltip(chart) {
            const $ = window.$;

            var tooltipSelector = '.ct-point';
            if (chart instanceof Chartist.Bar) {
                tooltipSelector = '.ct-bar';
            } else if (chart instanceof Chartist.Pie) {
                tooltipSelector = '[class^=ct-slice]';
            }

            var $chart = $(chart.container),
                $toolTip = $chart
                .append('<div class="ct-tooltip"></div>')
                .find('.ct-tooltip')
                .hide();

            $chart.on('mouseenter', tooltipSelector, function() {
                var $point = $(this),
                    seriesName = $point.parent().attr('ct:series-name'),
                    tooltipText = '';

                if (options.seriesName && seriesName) {
                    tooltipText += seriesName + '<br>';
                }

                if ($point.attr('ct:meta')) {
                    tooltipText += $point.attr('ct:meta') + '<br>';
                }

                var value = $point.attr('ct:value') || '0';

                tooltipText += options.valueTransform(value);

                $toolTip.html(tooltipText).show();
            });

            $chart.on('mouseleave', tooltipSelector, function() {
                $toolTip.hide();
            });

            $chart.on('mousemove', function(event) {

                let offsetX =  ((event.offsetX || event.originalEvent.layerX) - $toolTip.width() / 2) / .65 + 20;
                let offsetY =  ((event.offsetY || event.originalEvent.layerY) - $toolTip.height() / 2) / .65 - 20;
                $toolTip.css({
                    transform:`matrix(1,0,0,1,${offsetX},${offsetY})`

                });
            });

        };

    };


export default ToolTip;