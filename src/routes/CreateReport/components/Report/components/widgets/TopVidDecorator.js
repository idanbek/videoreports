import React from 'react'
import Isvg from 'react-inlinesvg'


export default (props) => {
    let { decorator: { type, src, style, className }, themeColors } = props;

    switch(type){
        case 'svg':
            return <Isvg src={ src } cacheGetRequests={ true } style={{fill: themeColors[0]}}/>;
        case 'div':
            return <div className={ className } style={ style }/>;
        default:
            return null;
    }
}