import React from 'react'
import { Textfit } from 'react-textfit'


class AutoFitInput extends React.Component {

    constructor(props) {
        super(props);
        this.handleFitText = this.handleFitText.bind(this);
    }

    handleFitText(newFontSize) {
        let { label: { style: { fontSize } } , changeFontSize } = this.props;
        newFontSize = `${newFontSize}px`;
        if (fontSize !== newFontSize) {
            changeFontSize(newFontSize);
        }
    }

    render() {
        let {
            onFocus , label: {
                maxFontSize , text , isCustomFontSize , timePicker , style: { transform , height , width , boxShadow , clipPath , zIndex , ...textStyles } ,
                themeStyle = {}
            } ,
            onHandleChange , themeColors , onBlur
        } = this.props;

        const ThemeStyleArr = Object.entries(themeStyle).map(([ key , value ]) => {
            return [ key , themeColors[ value ] ]
        });

        if (ThemeStyleArr.length > 0) {
            themeStyle = {};
            ThemeStyleArr.forEach(([ key , value ]) => {
                themeStyle[ key ] = value
            });
        }

        return (
            <div className="widgetLabel" onClick={onFocus}
                 style={{ transform , boxShadow , height , width , zIndex , clipPath , ...themeStyle }}>

                {/*TODO Move this to external function , we dotn need a div for each text.*/}
                {/*{ !isCustomFontSize &&*/}
                {/*<Textfit*/}
                {/*mode="single"*/}
                {/*max={ maxFontSize || 60 }*/}
                {/*style={{*/}
                {/*position:'absolute',*/}
                {/*width, height,*/}
                {/*fontFamily: textStyles['fontFamily'],*/}
                {/*fontWeight: textStyles['fontWeight']*/}
                {/*}}*/}
                {/*onReady={ this.handleFitText }>*/}
                {/*{ text }*/}
                {/*</Textfit>*/}
                {/*}*/}


                <textarea
                    spellCheck={false}
                    value={text}
                    disabled={timePicker}
                    rows={1}
                    onBlur={onBlur}
                    style={{ ...textStyles }}
                    onChange={onHandleChange.bind('text')}/>

            </div>
        )
    }
}

export default AutoFitInput;