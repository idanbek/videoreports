import React from 'react'
import ChartistGraph from 'react-chartist'
import chartOptions from './chartOptions'

export default function TopVidGraph({ graph:{ style }, data:{ fields=[], data=[] }={} }) {
    let series = [[], [], [], []], labels = [];

    data.forEach(d => {
        labels.push(d['date_start']);
        fields.forEach((field, idx) => {
            series[idx].push(d[field.value]);
        });
    });

    return (
        <div className="widgetImage" style={ style }>
            <div className="graphMap">
                { fields.map((field, idx) =>
                    <div key={ idx } className="graphKey" >
                        <i className="fa fa-circle"/>
                        { field.label }
                    </div>
                )}
            </div>

            <ChartistGraph style={{height: style.height, width: style.width}}
                           data={{ labels, series }}
                           options={ chartOptions }
                           type="Line"/>

        </div>
    );
}