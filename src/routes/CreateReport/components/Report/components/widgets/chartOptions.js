import Chartist from 'chartist';

function ctPointLabels(options) {
    return function ctPointLabels(chart) {
        let defaultOptions = {
            labelClass: 'ct-label',
            labelOffset: {x: 0, y: -20},
            textAnchor: 'middle'
        };

        options = {...defaultOptions , ...options};

        if (chart instanceof Chartist.Line) {
            chart.on('draw', function(data) {
                if(data.type === 'point') {
                    data.group.elem('text', {
                        x: data.x + options.labelOffset.x,
                        y: data.y + options.labelOffset.y,
                        style: 'text-anchor: ' + options.textAnchor
                    }, 'ct-custom-label').text(data.value.y.toFixed(2));
                }
            });
        }
    }
}

export default {
    axisX: {
        labelOffset: {x: -30, y: 5},
        labelInterpolationFnc: function(value, index) {
            let [ , months, day ] = value.split('-');
            return `${months}/${day}`
        }
    },
    axisY: {
        showLabel: false,
        scaleMinSpace: 25
    },
    chartPadding: {
        right: 40,
        left: 10,
        bottom: 40,
        top: 100
    },
    fullWidth: true,
    plugins: [
        ctPointLabels()
        //ToolTip()
    ]
};