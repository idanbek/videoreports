import React from 'react'
import Draggable from 'react-draggable'


export default (props) => {
    let { image:{ src, coordinates:{ x, y, zoom }, iconID=false, style }, onDrag, onFocus, isSelected } = props;


    return (
        <div className={ `widgetImage${isSelected ? ' imageSelected' : ''}${iconID ? ' noDrag' : ''}` }
             style={ style }
             onClick={ onFocus }>

            { iconID?
                <img src={ src }
                         style={{transform: `matrix(${zoom}, 0, 0, ${zoom}, ${x}, ${y})` }}
                         alt=""/>:
                <Draggable position={{x, y}} onDrag={ onDrag }>
                <span>
                    <img src={ src }
                         style={{transform: `matrix(${zoom}, 0, 0, ${zoom}, ${x}, ${y})` }}
                         alt=""/>
                </span>
            </Draggable>
            }
        </div>
    );
}
