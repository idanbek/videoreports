import React from 'react'
import { isEqual, get, set, cloneDeep } from 'lodash'
import Spinner from 'react-spinkit'
import measureText from 'measure-text'

// import TopVidLabel from './components/TopVidLabel'
import TopVidImage from './widgets/TopVidImage'
import TopVidGraph from './widgets/TopVidGraph'
import TopVidDecorator from './widgets/TopVidDecorator'
import TopVidLabel from './widgets/AutoFitInput'


class SimpleWidget extends React.PureComponent{

    static getTextSize(text, {fontFamily, lineHeight, fontSize, width, height}) {
        let measurement = measureText({
            text,
            lineHeight,
            fontFamily,
            fontSize
        });

        let parsedFontSize = parseFloat(fontSize.replace('px', '')),
            elemWidth = Number(width.slice(0, -2));

        while (measurement['width']['value'] > elemWidth && parsedFontSize > 8) {
            parsedFontSize -= 1;
            measurement = measureText({
                text,
                fontFamily,
                lineHeight,
                fontSize: parsedFontSize + 'px',
            });
        }

        return parsedFontSize
    }

    constructor(props){
        super(props);

        this.handleLabelOnBlur = this.handleLabelOnBlur.bind(this);
        this.changeFontSize = this.changeFontSize.bind(this);
         this.changeLabel =  this.changeLabel.bind(this);
         this.changeFontSize = this.changeFontSize.bind(this);
         this.changeImage = this.changeImage.bind(this);

    }

    changeLabel(index, event) {
        let { setIn , widgetIdx, widget:{ labels }, widgetData, addWidgetData } = this.props,
            label = cloneDeep(labels[index]);

        const IS_VALUE = index % 2;

        label['text'] = event.target.value;
        // if widget has data and changing widget data label need change data also
        if (widgetData && index < widgetData.data.length) {
            let newWidgetData = cloneDeep(widgetData);
            label['isEdited'] = true;

            if (IS_VALUE) {
                set(newWidgetData, ['data', index - 1, 'speech_text'], label['text']);
            } else {
                set(newWidgetData, ['data', index, 'label'], label['text']);
            }
            addWidgetData(newWidgetData)
        }
        setIn(['frames', widgetIdx, 'labels', index], label);
    }

    changeFontSize(index, newFontSize) {
        let { setIn, widgetIdx } = this.props;
        setIn(['frames', widgetIdx, 'labels', index, 'style', 'fontSize'], newFontSize);
    }

    changeImage(index, {zoom}, event, {x, y}) {
        let { setIn, widgetIdx } = this.props;
        event.preventDefault();
        event.stopPropagation();
        setIn(['frames', widgetIdx, 'images', index, 'coordinates'], {x, y, zoom});
    }

    handleLabelOnBlur() {
        let { changeVoiceOver, widget:{ voiceOver }, widgetPath } = this.props;
        changeVoiceOver(cloneDeep(voiceOver), widgetPath);
    }

    render() {
        let { widgetPath=[],style={}, focusPath=[], widgetData, themeColors, handleFocus=()=>{},
              widget: { labels, graph, images, decorators, size:{ height, width } } } = this.props,
            isLoading = get(widgetData, ['status', 'isLoading']);

        return (
            <div className="simpleWidget"
                 onClick={ (event)=>{handleFocus({type: 'widget', path: widgetPath},event)} }
                 style={{...style,height: `${height * 270}px`, width: `${width * 480}px`}}>

                { isLoading ?
                    <Spinner className="loader dataSpinner" name="chasing-dots" color="#6D50E4"/>
                    :
                    <div>
                        { labels.map((label, idx) => {
                            let focusItem = label.timePicker ? {type: 'date', path: 'dates'} :
                                {type: 'text', path: widgetPath.concat(['labels', idx])};

                            return (
                                <TopVidLabel key={ idx }
                                             isSelected={ isEqual(focusItem.path, focusPath) }
                                             onBlur={ this.handleLabelOnBlur }
                                             onFocus={(event)=>{handleFocus(focusItem,event)} }
                                             changeFontSize={ (newFontSize)=>{this.changeFontSize(idx,newFontSize) }}
                                             onHandleChange={ (event)=>{ this.changeLabel(idx,event) } }
                                             label={ label }
                                             themeColors={ themeColors }/>
                            );
                        })}

                        { images.map((image, idx) => {
                            let path = widgetPath.concat(['images', idx]),
                                type = image.iconID ? 'icon' : 'image';
                            return (
                                <TopVidImage key={ idx }
                                             image={ image }
                                             onFocus={ (event)=>{handleFocus({ type,path},event)} }
                                             isSelected={ isEqual(path, focusPath) }
                                             onDrag={ (event,coordinates)=>{this.changeImage(idx,image.coordinates,event,coordinates)}} />
                            )
                        })}
                    </div>
                }

                { decorators.map((decorator, idx) =>
                    !(decorator.isLoading) &&
                        <TopVidDecorator key={ idx }
                                         decorator={ decorator }
                                         themeColors={ themeColors }/>
                )}

                { graph && <TopVidGraph graph={ graph } data={ widgetData }/>}
            </div>
        )
    }
}

export default SimpleWidget;