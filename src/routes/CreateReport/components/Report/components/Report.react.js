import React from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { size } from 'lodash'
import ReactGridLayout from 'react-grid-layout'

import SimpleWidget from './SimpleWidget.react'
import { setFocusItem } from '../../../../../state/focusItem/actions'
import { addWidgetData } from '../../../../../state/widgetDatas/actions'
import { STORAGE_KEY , WIDGET_DATAS } from '../../../../../constants/localStorage'
import { setIn , deleteWidget , setPositions , changeVoiceOver } from '../../../../../state/activeVideo/actions'
import { sleep } from "../../../../../utils/utilFunctions";


class Report extends React.PureComponent {

    static WRAPPING_ELEMENT_ID = 'Builder' ;


    static toDataGridObject(widget) {
        let { id: i , size: { height: h , width: w } , position: { x , y } , staticWidget } = widget;
        return { i: i.toString() , x , y , w , h , 'static': staticWidget }
    }

    constructor(props) {
        super(props);
        let { activeVideo: { frames } } = this.props;

        this.lastLayout = frames.map(widget => Report.toDataGridObject(widget));
        this.onUnload = this.onUnload.bind(this);
        this.handleFocus = this.handleFocus.bind(this);
        this.onLayoutChange = this.onLayoutChange.bind(this);
    }

    onUnload() {
        let { widgetDatas , activeVideo } = this.props;
        // localStorage.setObject(STORAGE_KEY, activeVideo);
        // localStorage.setObject(WIDGET_DATAS, widgetDatas);
    }

    componentDidMount() {
       window.addEventListener('beforeunload', this.onUnload)
    }

    componentWillUnmount() {
        this.onUnload();
        window.removeEventListener('beforeunload', this.onUnload)
    }

    handleFocus(focusItem, event) {
        event.stopPropagation();
        let { setFocusItem } = this.props;
        setFocusItem(focusItem);
    }

    async onLayoutChange(layout) {
        // Update positions in frames.
        let { setPositions } = this.props;
        setPositions(layout);

        // Check if a new widget was added.
        const WIDGET_WAS_ADDED = size(layout) > size(this.lastLayout);

        // If it is, scroll to the bottom.
        if (WIDGET_WAS_ADDED) {
            await sleep(100);
            let container = document.getElementById(Report.WRAPPING_ELEMENT_ID);
            container.scrollTop = container.scrollHeight;
        }

        // Save the last layout.
        this.lastLayout = { ...layout };
    }

    render() {
        let { setIn, changeVoiceOver, focusPath, deleteWidget, widgetDatas, addWidgetData,  activeVideo:
            { bg, frames } } = this.props;

        return (
            <div className="frames-wrapper main-report"
                 style={{backgroundImage: `url(${bg.backgroundImage})${bg.backgroundGradient? `,${bg.backgroundGradient}`:''}`}}>
                <ReactGridLayout onLayoutChange={ this.onLayoutChange }
                                 width={ 1920 }
                                 cols={ 4 }
                                 draggableCancel="input,textarea"
                                 margin={ [0, 0] }
                                 rowHeight={ 270 }
                                 isResizable={ false }
                                 useCSSTransforms={ true }>
                    { frames.map((widget, idx)=> {
                        const isFocused = focusPath.length === 2 && focusPath[1] === idx && focusPath[0] === 'frames';
                        return (
                            <div key={ widget.id }
                                 className={ `grid-wrapper ${isFocused ? 'active' : ''}` }
                                 data-grid={ Report.toDataGridObject(widget) }>
                                { !widget.staticWidget &&
                                    <button type="button"
                                        onClick={ deleteWidget.bind(this,widget.id) }
                                        className="btn delete-widget-button">
                                        <i className="fa fa-times"/>
                                    </button>
                                }

                                <SimpleWidget data-grid={ Report.toDataGridObject(widget) }
                                              widgetIdx={ idx }
                                              focusPath={ focusPath }
                                              addWidgetData={ addWidgetData }
                                              changeVoiceOver={ changeVoiceOver }
                                              widgetData={ widgetDatas[widget.id] }
                                              widgetPath={ ['frames', idx] }
                                              handleFocus={ this.handleFocus }
                                              setIn={ setIn }
                                              themeColors={ bg.themeColors }
                                              widget={ widget }/>
                            </div>
                        )
                    })}
                </ReactGridLayout>
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        focusPath: state.focusItem.path,
        activeVideo: state.activeVideo,
        widgetDatas: state.widgetDatas,
    };
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        setIn ,
        deleteWidget ,
        setPositions ,
        setFocusItem ,
        changeVoiceOver ,
        addWidgetData
    }, dispatch);
}


export default connect(mapStateToProps , mapDispatchToProps)(Report);
