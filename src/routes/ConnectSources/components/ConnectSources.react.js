import React, { Component } from 'react'
import DataSource from './DataSource.react'
import {connect} from 'react-redux'
import {bindActionCreators} from 'redux'

import {fetchDataSources} from '../../../state/dataSources/actions'

import allIntegrations from '../../../constants/allIntegrations'

class ConnectSources extends Component {

    componentWillMount() {
        let {  fetchDataSources } = this.props;
        fetchDataSources();
    }

    render() {
        let { dataSources } = this.props;

        const DataSourceBoxes = allIntegrations.slice(1).map((integration,index) =>
           <DataSource key={index}
                       integrationName={integration.name}
                       redirect={integration.redirect}
                       imageSrc={integration.imageSrc}
                       isActive={dataSources.includes(integration.id)} />
        );

        return (
            <div>
                {DataSourceBoxes}
            </div>
        );
    }
}


function mapStateToProps(state) {
    return {
        dataSources: state.dataSources
    };
}
function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        fetchDataSources
    }, dispatch);
}
export default connect(mapStateToProps, mapDispatchToProps)(ConnectSources);