import React, {Component} from 'react';
import axios from 'axios';
import {Button,Label} from 'react-bootstrap';

import {BASE_URL} from "../../../constants/urls";


class DataSource extends Component {

    static FB_SDK = window.FB;
    static FB_CONFIG = {
         appId: '1007349092734783',
        status: true,
        cookie: true,
        xfbml: true,
        version: 'v2.10'
    };
    static FB_PREMISSIONS = 'ads_management';

    componentDidMount(){
        DataSource.FB_SDK.init(DataSource.FB_CONFIG);
    }

    connectFacebook(){
            let { redirect } = this.props;

            DataSource.FB_SDK.login((response)=> {
                if (response.hasOwnProperty('authResponse') && response['authResponse'].hasOwnProperty('accessToken')) {
                        let access_token = response['authResponse']['accessToken'];
                        axios.post(BASE_URL+ redirect,{access_token:access_token}).then((response)=>{

                        })
                }
        }, {scope: DataSource.FB_PREMISSIONS});
    }


    render() {
        const {imageSrc,redirect,isActive,integrationName} = this.props;
        let DisplayButton = null;

        if(isActive){
            DisplayButton = <Label bsStyle="success" href={redirect}>Added</Label>;
        }else if(integrationName === 'Facebook'){
             DisplayButton = <Button bsStyle="default" onClick={this.connectFacebook.bind(this)}>Connect</Button>
        }else{
              DisplayButton =  <Button bsStyle="default" href={redirect}>Connect</Button>;
        }


        return (
            <div className="DataSource" style={{border:`4px solid ${isActive? 'green':'whitesmoke'}`}}>
                <div style={{background:`url(${imageSrc})`,backgroundSize:'contain',backgroundRepeat:'no-repeat'}} alt="" className="data-source-image"/>
                <div className="buttonHolder">
                    {DisplayButton}
                </div>
            </div>
        );
    }
}

export default DataSource;
