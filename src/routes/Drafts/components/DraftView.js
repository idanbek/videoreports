import React from 'react'
import { connect } from 'react-redux'
import axios from 'axios'
import { withRouter } from 'react-router-dom'

// Local imports
import {GET_DRAFTS_URL} from "../../../constants/urls";
import UserVideo from '../../../shared/UserVideo.react'
import { bindActionCreators } from 'redux'
import {
    loadVideo
} from '../../../state/activeVideo/actions'


class DraftView extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            drafts: []
        }
    }

     async componentDidMount() {
        const {data:drafts} = await axios.get(GET_DRAFTS_URL);
        this.setState({drafts})
    }

    render() {
        let { history: { push }, loadVideo } = this.props,
            { drafts } = this.state;

        return (
            <div id='DraftsWrapper'>
                <div id='draftsTitle'>My Drafts</div>
                {drafts.sort(({date:a},{date:b})=>b-a).map((draft, idx) =>
                    <UserVideo
                        onPublish={
                            (videoJson) => {
                                loadVideo(videoJson);
                                push('/publish')
                            }
                        }
                        onCustomize={(videoJson) => {
                                loadVideo(videoJson);
                                push('/')

                            }}
                        videoID={draft.id}
                        draft={draft.video_json}
                        date={draft.date}
                        key={idx}/>
                )}
            </div>
        );
    }
}



function mapStateToProps(state) {
    return {};
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        loadVideo
    }, dispatch);
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(DraftView));
