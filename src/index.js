import React from 'react'
import ReactDOM from 'react-dom'
import { Provider } from 'react-redux'
import { HashRouter } from 'react-router-dom'
import { createStore, applyMiddleware } from 'redux'
import promise from 'redux-promise'
import thunk from 'redux-thunk'
import reducers from './state/reducer'

import './styles/base.css'
import Root from './routes/Root'

const createStoreWithMiddleware = applyMiddleware(thunk, promise)(createStore);

ReactDOM.render((
    <Provider store={createStoreWithMiddleware(reducers)}>
        <HashRouter>
            <Root/>
        </HashRouter>
    </Provider>
), document.getElementById('root'));