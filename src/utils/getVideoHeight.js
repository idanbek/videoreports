export function getVideoHeight(frames){
    return `${Math.max(...frames.map(({size: {height}, position: {y}}) => y + height)) * 270}px`;
}