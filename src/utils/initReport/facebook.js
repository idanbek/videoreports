import { merge } from 'lodash'
import moment from 'moment'
import ALL_METRICS from '../../constants/allMetrics'
import { getWidget , DEFAULT_FACEBOOK_FIELDS,NO_DATA_WIDGETS } from '../../constants/allWidgets'
import { WIDGET_ICONS_URL } from '../../constants/urls'
import { DEFAULT_VOICE , DEFAULT_CODE } from '../../constants/pollyMapping'
import { TIME_RANGES , DATE_FORMAT } from "../../constants/createReportNavs";

export default function (accName , accID , campID , ads) {
    let widgets = [] , widgetDatas = {};
    const widgetFields = {};

    //
    //  Main title POSITION: {x:0,y:0}
    //      Speech:
    //      Hello {account name} welcome to your facebook ads video report.
    //      This report covers the preformance of your campaign during the {last_week TODO Make date placeholder. }
    const mainTitleDefaultText = `Hello ${accName} - and welcome to your weekly video report! This report covers the preformance of your campaign during the last week`
    const mainTitle = {
        ...getWidget('Main title') ,
        position: { x: 0 , y: 0 } ,
        voiceOver: {
            src: null ,
            text: mainTitleDefaultText ,
            languageCode: DEFAULT_CODE ,
            voice: DEFAULT_VOICE ,
            template: mainTitleDefaultText ,
        }
    };
    let [ startDate , endDate ] = TIME_RANGES[ 0 ][ 1 ];
    mainTitle[ 'labels' ].find(label => label.timePicker)[ 'text' ] = `${moment(startDate , DATE_FORMAT).format('D.M')}-${moment(endDate , DATE_FORMAT).format('D.M')}`;

    //
    // Double Metric POSITION: {x:0,y:1}
    // Title = Overview
    // Metrics = Reach,spend
    // Speech:
    //          Overall , the campaign reached {value[0]} people. And the total cost was {value[1]}
    const doubleMetricDefaultText = `Overall , the campaign reached [metric[0].value] people. And the total cost was [metric[1].value]`;
    const doubleMetric = {
        ...getWidget('Double Metric') ,
        position: { x: 0 , y: 1 } ,
        voiceOver: {
            text: '' ,
            languageCode: DEFAULT_CODE ,
            voice: DEFAULT_VOICE ,
            template: doubleMetricDefaultText
        }
    };
    widgetFields[ doubleMetric.id ] = [ 'reach' , 'spend' ];

    //
    // Notebook metric POSITION: {x:2,y:1}
    // Title = Engagement <- no S
    // Metrics = Clicks,likes,shares/comments
    // Speech:
    //          Now lets see how users engaged with your campaign. The ads in the campaign were clicked {value[0]}
    //          , liked {value[1]} , and the number of shares was {value[2]}.
    const noteBooDefaultText = `Now lets see how users engaged with your campaign. The ads in the campaign were clicked [metric[0].value], liked [metric[1].value] , and the number of shares was [metric[2].value].`;
    const noteBook = {
        ...getWidget('Metric Notebook') ,
        position: { x: 2 , y: 1 } ,
        voiceOver: {
            text: '' ,
            languageCode: DEFAULT_CODE ,
            voice: DEFAULT_VOICE ,
            template: noteBooDefaultText
        }
    };
    widgetFields[ noteBook.id ] = [ 'clicks' , 'actions.like' , 'actions.comment' ];

    // Chart POSITION: {x:0,y:2}
    // Title = Overview
    // Metrics = CPC
    // Speech:
    //          The chart belows shows the number of clicks per day.
    //          Looks like see we are moving in the right direction.
    const chartDefaultText = `The chart belows shows the number of clicks per day. Looks like we are moving in the right direction.`;
    const chart = {
        ...getWidget('Chart') ,
        position:{x:0,y:2},
        voiceOver:{
            src:null,
            text:'',
            languageCode:DEFAULT_CODE,
            voice:DEFAULT_VOICE,
            template:chartDefaultText
        }
    };
    widgetFields[ chart.id ] = ['clicks'];

    if(!ads.isEmpty()){

        // Title POSITION: {x:0,y:3} IF ADS EXISTS
        // Title = Top ads
        // Speech:
        //          Now its time to look at the best preforming ads in this campaign.
        const adsTitleDefaultText = `Now its time to look at the best preforming ads in this campaign.`
        const adsTitle = {
            ...getWidget('Title') ,
            position: { x:0 , y: 4 } ,
            voiceOver: {
                src:null,
                text: '' ,
                languageCode: DEFAULT_CODE ,
                voice: DEFAULT_VOICE ,
                template: adsTitleDefaultText
            }
        };
        adsTitle['labels'][0]['text'] = 'Top ads';
        widgets.push(adsTitle)


        // Ad 1 : POSITION: {x:0,y:4} IF ADS 1  EXISTS
        //  Metrics= impressions, clicks , CPC
        //  Speech= This ad did great! with [(name, value) in metrics ].

        // Ad 2 : POSITION: {x:2,y:4} IF ADS 2 EXISTS
        //  Metrics= impressions, clicks , CPC
        //  Speech= Another top-preformer was this ad. With some impressive results.
        const adsDefaultText = [
            'This ad did great! with [metric[0].value] in [metric[0].name] , [metric[1].value] in [metric[1].name] and [metric[2].value] in [metric[2].name]',
            'Another top-preformer was this ad. With some impressive results'
        ];

        for(let i = 0; i < Math.min(ads.length,2); i++){
            let ad = {
                ...getWidget('Ad Box'),
                position:{x: i*2 ,y:5},
                voiceOver:{
                     text: '' ,
                    languageCode: DEFAULT_CODE ,
                    voice: DEFAULT_VOICE ,
                    template:adsDefaultText[i]
                }
            };
            widgetFields[ad.id] = ['impressions','clicks','cpp']
            widgets.push(ad);
        }
    }

    // Title POSITION: {x:0,y:BOTTOM}
    // Title = Thank you for watching.
    // Speech:
    //          Dear {Account name }- Thank you very much for watching this report.
    //          Should you need any further information, please do not hesitate to contact us.
    const endTitleDefaultText = `Dear ${accName}- Thank you very much for watching this report. Should you need any further information, please do not hesitate to contact us`;
    const endTitle = {
            ...getWidget('Title') ,
            position: { x:0 , y: ads.isEmpty()? 4: 8} ,
            voiceOver: {
                src:null,
                text: '' ,
                languageCode: DEFAULT_CODE ,
                voice: DEFAULT_VOICE ,
                template: endTitleDefaultText
            }
        };
        endTitle['labels'][0]['text'] = 'Thank you for watching';



    widgets= [ mainTitle , doubleMetric , noteBook, chart ,endTitle, ...widgets ];
    // Add data
    widgets
        .filter(({ name }) => !NO_DATA_WIDGETS.includes(name))
        .forEach(({ id }) => {
            console.log('ID' , id)
            widgetDatas[ id ] = {
                widgetId: id ,
                level: 'campaign' ,
                campaign_id: campID ,
                item_id: campID ,
                account_id: accID ,
                fields: widgetFields[ id ]
                    .map((fieldValue) => ALL_METRICS
                        .find(({ value }) => value === fieldValue))
            }
        });

    // Must be set to one for graph.
    widgetDatas[chart.id]['time_increment'] = 1;








    return { widgets , widgetDatas }
}